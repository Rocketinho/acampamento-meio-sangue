﻿namespace Acampamento_Meio_Sangue
{
    partial class TelaMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TelaMenu));
            this.mnStrip = new System.Windows.Forms.MenuStrip();
            this.cadrastrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atividadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cronogramaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.semiDeusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.semiDeusToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chaléToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.semideusToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.lblX = new System.Windows.Forms.Label();
            this.mnStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnStrip
            // 
            this.mnStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadrastrosToolStripMenuItem,
            this.consultaToolStripMenuItem});
            this.mnStrip.Location = new System.Drawing.Point(0, 0);
            this.mnStrip.Name = "mnStrip";
            this.mnStrip.Size = new System.Drawing.Size(373, 24);
            this.mnStrip.TabIndex = 0;
            this.mnStrip.Text = "menuStrip1";
            // 
            // cadrastrosToolStripMenuItem
            // 
            this.cadrastrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.atividadeToolStripMenuItem,
            this.cronogramaToolStripMenuItem1,
            this.semiDeusToolStripMenuItem,
            this.semiDeusToolStripMenuItem1});
            this.cadrastrosToolStripMenuItem.Name = "cadrastrosToolStripMenuItem";
            this.cadrastrosToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.cadrastrosToolStripMenuItem.Text = "Cadrastros";
            // 
            // atividadeToolStripMenuItem
            // 
            this.atividadeToolStripMenuItem.Name = "atividadeToolStripMenuItem";
            this.atividadeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.atividadeToolStripMenuItem.Text = "Atividade";
            this.atividadeToolStripMenuItem.Click += new System.EventHandler(this.atividadeToolStripMenuItem_Click);
            // 
            // cronogramaToolStripMenuItem1
            // 
            this.cronogramaToolStripMenuItem1.Name = "cronogramaToolStripMenuItem1";
            this.cronogramaToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.cronogramaToolStripMenuItem1.Text = "Cronograma";
            this.cronogramaToolStripMenuItem1.Click += new System.EventHandler(this.cronogramaToolStripMenuItem1_Click);
            // 
            // semiDeusToolStripMenuItem
            // 
            this.semiDeusToolStripMenuItem.Name = "semiDeusToolStripMenuItem";
            this.semiDeusToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.semiDeusToolStripMenuItem.Text = "Deus";
            this.semiDeusToolStripMenuItem.Click += new System.EventHandler(this.semiDeusToolStripMenuItem_Click);
            // 
            // semiDeusToolStripMenuItem1
            // 
            this.semiDeusToolStripMenuItem1.Name = "semiDeusToolStripMenuItem1";
            this.semiDeusToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.semiDeusToolStripMenuItem1.Text = "SemiDeus";
            this.semiDeusToolStripMenuItem1.Click += new System.EventHandler(this.semiDeusToolStripMenuItem1_Click);
            // 
            // consultaToolStripMenuItem
            // 
            this.consultaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chaléToolStripMenuItem,
            this.deusToolStripMenuItem,
            this.semideusToolStripMenuItem2});
            this.consultaToolStripMenuItem.Name = "consultaToolStripMenuItem";
            this.consultaToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.consultaToolStripMenuItem.Text = "Consultas";
            // 
            // chaléToolStripMenuItem
            // 
            this.chaléToolStripMenuItem.Name = "chaléToolStripMenuItem";
            this.chaléToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.chaléToolStripMenuItem.Text = "Chale";
            this.chaléToolStripMenuItem.Click += new System.EventHandler(this.chaléToolStripMenuItem_Click);
            // 
            // deusToolStripMenuItem
            // 
            this.deusToolStripMenuItem.Name = "deusToolStripMenuItem";
            this.deusToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.deusToolStripMenuItem.Text = "Deus";
            this.deusToolStripMenuItem.Click += new System.EventHandler(this.deusToolStripMenuItem_Click);
            // 
            // semideusToolStripMenuItem2
            // 
            this.semideusToolStripMenuItem2.Name = "semideusToolStripMenuItem2";
            this.semideusToolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.semideusToolStripMenuItem2.Text = "Semideus";
            this.semideusToolStripMenuItem2.Click += new System.EventHandler(this.semideusToolStripMenuItem2_Click);
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.BackColor = System.Drawing.Color.White;
            this.lblX.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblX.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblX.ForeColor = System.Drawing.Color.Black;
            this.lblX.Location = new System.Drawing.Point(355, 0);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(18, 19);
            this.lblX.TabIndex = 12;
            this.lblX.Text = "X";
            this.lblX.Click += new System.EventHandler(this.lblX_Click);
            // 
            // TelaMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackgroundImage = global::Acampamento_Meio_Sangue.Properties.Resources.Camp_half_blood;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(373, 315);
            this.ControlBox = false;
            this.Controls.Add(this.lblX);
            this.Controls.Add(this.mnStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mnStrip;
            this.Name = "TelaMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tela Menu";
            this.mnStrip.ResumeLayout(false);
            this.mnStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnStrip;
        private System.Windows.Forms.ToolStripMenuItem cadrastrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem semiDeusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem semiDeusToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cronogramaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem chaléToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem atividadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deusToolStripMenuItem;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.ToolStripMenuItem semideusToolStripMenuItem2;
    }
}