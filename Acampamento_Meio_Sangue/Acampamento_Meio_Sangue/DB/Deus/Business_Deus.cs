﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.Deus
{
    class Business_Deus
    {
        public int Salvar(DTO_Deus dto)
        {
            if (dto.Foto == null)
            {
                throw new ArgumentException("Preencha o campo de Foto");
            }
            else
            {
                
               Database_Deus database = new Database_Deus();
               return database.Salvar(dto);
                
            }
        }

        public void Alterar(DTO_Deus dto)
        {
            Database_Deus database = new Database_Deus();
            database.Alterar(dto);
        }

        public void Remover(int Id)
        {
            Database_Deus database = new Database_Deus();
            database.Remover(Id);
        }

        public List<DTO_Deus> Consultar(DTO_Deus dto)
        {
            Database_Deus database = new Database_Deus();
            return database.Consultar(dto);
        }

        public List<DTO_Deus> Listar()
        {
            Database_Deus database = new Database_Deus();
            List<DTO_Deus> list = database.Listar();

            return list;
        }

        public bool Logar (DTO_Deus dto)
        {
            Business_Deus db = new Business_Deus();
            return db.Logar(dto);
        }

        public bool VerificarRegistro(DTO_Deus dto)
        {
            Database_Deus db = new Database_Deus();
            return db.VerificarRegistro(dto);
        }


    }
}
