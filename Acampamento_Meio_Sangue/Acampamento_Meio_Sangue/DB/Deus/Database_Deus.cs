﻿using Acampamento_Meio_Sangue.DB._Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.Deus
{
    class Database_Deus
    {
        public int Salvar(DTO_Deus dto)
        {
            string script =
                @"INSERT INTO tb_deus(nm_deus, ds_deus, ds_habilidade, ds_foto)
                VALUES(@nm_deus, @ds_deus, @ds_habilidade, @ds_foto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_deus", dto.Deus));
            parms.Add(new MySqlParameter("ds_deus", dto.Descrição));
            parms.Add(new MySqlParameter("ds_habilidade", dto.Habilidade));
            parms.Add(new MySqlParameter("ds_foto", dto.Foto));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Alterar(DTO_Deus dto)
        {
            string script =
                @"UPDATE tb_deus SET nm_deus = @nm_deus,
                                          ds_deus = @ds_deus,
                                          ds_habilidade = @ds_habilidade
                                          ds_foto = @ds_foto
                WHERE id_deus = @id_deus";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_deus", dto.ID_Deus));
            parms.Add(new MySqlParameter("nm_deus", dto.Deus));
            parms.Add(new MySqlParameter("ds_deus", dto.Descrição));
            parms.Add(new MySqlParameter("ds_habilidade", dto.Habilidade));
            parms.Add(new MySqlParameter("ds_foto", dto.Foto));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(int Id)
        {
            string script =
                 @"DELETE FROM tb_deus WHERE id_deus = @id_deus";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_deus", Id));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<DTO_Deus> Consultar(DTO_Deus dto)
        {
            string script =
                @"SELECT * FROM vw_consultar_deus WHERE nm_deus like @nm_deus";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_deus", dto.Deus));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Deus> dtos = new List<DTO_Deus>();
            while (reader.Read())
            {
                DTO_Deus dt = new DTO_Deus();
                dt.ID_Deus = reader.GetInt32("id_deus");
                dt.Deus = reader.GetString("nm_deus");
                dt.Descrição = reader.GetString("ds_deus");
                dt.Habilidade = reader.GetString("ds_habilidade");
                if(dt.Foto == null)
                {

                }
                else
                {
                    dt.Foto = reader.GetString("ds_foto");

                }

                dtos.Add(dt);


            }
            reader.Close();
            return dtos;
        }

        public List<DTO_Deus> Listar()
        {
            string script =
                @"SELECT * FROM tb_deus";
            List<MySqlParameter> parm = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Deus> fora = new List<DTO_Deus>();

            while (reader.Read())
            {
                DTO_Deus dentro = new DTO_Deus();
                dentro.ID_Deus = reader.GetInt32("id_deus");
                dentro.Deus = reader.GetString("nm_deus");
                dentro.Descrição = reader.GetString("ds_deus");
                dentro.Habilidade = reader.GetString("ds_habilidade");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }

        public bool VerificarRegistro(DTO_Deus dto)
        {
            string script = @"SELECT * FROM tb_deus WHERE nm_deus = @nm_deus ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_deus", dto.Deus));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            if(reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }

            

        }

        

    }
}
