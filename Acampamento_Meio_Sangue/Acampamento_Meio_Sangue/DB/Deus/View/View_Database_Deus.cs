﻿using Acampamento_Meio_Sangue.DB._Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.DB.Deus.View
{
    class View_Database_Deus
    {
        public View_DTO_Deus Consultar (View_DTO_Deus dto)
        {
            string script =
                @"SELECT * FROM view_consultar_deus WHERE nm_deus = @nm_deus";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_deus", dto.Deus));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            reader.Read();
            

                View_DTO_Deus teste = new View_DTO_Deus();

                View_DTO_Deus dt = new View_DTO_Deus();
                dt.ID_Deus = reader.GetInt32("id_deus");
                dt.Deus = reader.GetString("nm_deus");
                teste.Foto = reader.GetString("ds_foto");
                if(teste.Foto == null)
                    
                {

                }
                else
                {
                dt.Foto = teste.Foto;
    
                }

                dt.Habilidade = reader.GetString("ds_habilidade");
                dt.Descrição = reader.GetString("ds_deus");
                
            
             reader.Close();
             return dt;
            
           //else
           // {
           //     throw new ArgumentException("Deu ruim campeão");
           // }
        }
       

        public bool VerificarRegistro(View_DTO_Deus dto)
        {
            string script = @"SELECT * FROM view_consultar_deus WHERE nm_deus = @nm_deus ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_deus", dto.Deus));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }



        }
    }
}
