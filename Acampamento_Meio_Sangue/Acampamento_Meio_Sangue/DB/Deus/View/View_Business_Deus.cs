﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.DB.Deus.View
{
    class View_Business_Deus
    {
        public View_DTO_Deus Consultar(View_DTO_Deus dto)
        {

            View_Database_Deus database = new View_Database_Deus();
            return database.Consultar(dto);

        }
        
        public bool VerificarRegistro(View_DTO_Deus dto)
        {
            View_Database_Deus db = new View_Database_Deus();
            return db.VerificarRegistro(dto);
        }
    }
}
