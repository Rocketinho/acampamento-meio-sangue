﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.DB.Deus
{
    class View_DTO_Deus
    {
        public int ID_Deus { get; set; }

        public string Deus { get; set; }

        public string Descrição { get; set; }

        public string Habilidade { get; set; }

        public string Foto { get; set; }

        public string Filho { get; set; }


    }
}
