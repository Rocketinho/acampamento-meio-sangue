﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.DB._Base
{
    class Connection
    {
        public MySqlConnection Create()
        {
            string connectionString = "server=localhost;database=Acampamento;uid=root;password=1234;sslmode=none";

            MySqlConnection conn = new MySqlConnection(connectionString);
            conn.Open();
            return conn;

        }
    }
}
