﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.Atividade
{
    class DTO_Atividade
    {
        public int ID_Atividade { get; set; }

        public string  Atividade { get; set; }

        public string Equipamento { get; set; }

        public string Professor { get; set; }

    }
}
