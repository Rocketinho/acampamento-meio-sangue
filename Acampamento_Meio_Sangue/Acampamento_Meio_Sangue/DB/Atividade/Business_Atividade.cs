﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.Atividade
{
    class Business_Atividade
    {
        public int Salvar(DTO_Atividade dto)
        {
            Database_Atividade database = new Database_Atividade();
            return database.Salvar(dto);
        }

        public void Alterar(DTO_Atividade dto)
        {
            Database_Atividade database = new Database_Atividade();
            database.Alterar(dto);
        }

        public void Remover(string atv)
        {
            Database_Atividade database = new Database_Atividade();
            database.Remover(atv);
        }

        public List<DTO_Atividade> Consultar(DTO_Atividade dto)
        {
            Database_Atividade database = new Database_Atividade();
            return database.Consultar(dto);
        }
        public List<DTO_Atividade> Listar()
        {
            Database_Atividade database = new Database_Atividade();
            return database.Listar();
        }

        public bool VerificarRegistro(DTO_Atividade dto)
        {
            Database_Atividade db = new Database_Atividade();
            return db.VerificarRegistro(dto);
        }
    }
}
