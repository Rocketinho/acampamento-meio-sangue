﻿using Acampamento_Meio_Sangue.DB._Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.Atividade
{
    class Database_Atividade
    {
        public int Salvar(DTO_Atividade dto)
        {
            string script =
                @"INSERT INTO tb_atividade(nm_atividade, ds_equipamento, nm_professor)
                VALUES(@nm_atividade, @ds_equipamento, @nm_professor)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_atividade", dto.Atividade));
            parms.Add(new MySqlParameter("ds_equipamento", dto.Equipamento));
            parms.Add(new MySqlParameter("nm_professor", dto.Professor));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Alterar(DTO_Atividade dto)
        {
            string script =
                @"UPDATE tb_atividade SET nm_atividade      = @nm_atividade,
                                          ds_equipamento   = @ds_equipamento,
                                          nm_professor     = @nm_professor,
                WHERE id_atividade = @id_atividade";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_atividade", dto.ID_Atividade));
            parms.Add(new MySqlParameter("nm_atividade", dto.Atividade));
            parms.Add(new MySqlParameter("ds_equipamento", dto.Equipamento));
            parms.Add(new MySqlParameter("nm_professor", dto.Professor));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(String atv)
        {
            string script =
                 @"DELETE FROM tb_atividade WHERE nm_atividade = @nm_atividade";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_atividade", atv ));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<DTO_Atividade> Consultar(DTO_Atividade dto)
        {
            string script =
                @"SELECT * FROM tb_atividade WHERE nm_atividade like @nm_atividade";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_atividade", dto.Atividade));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Atividade> dtos = new List<DTO_Atividade>();
            while(reader.Read())
            {
                DTO_Atividade dt = new DTO_Atividade();
                dt.ID_Atividade = reader.GetInt32("id_atividade");
                dt.Atividade = reader.GetString("nm_atividade");
                dt.Equipamento = reader.GetString("ds_equipamento");
                dt.Professor = reader.GetString("nm_professor");
                dtos.Add(dt);
            }
            reader.Close();
            return dtos;

        }
        public List<DTO_Atividade> Listar ()
        {
            string script =
                @"SELECT * FROM tb_atividade";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Atividade> dtos = new List<DTO_Atividade>();

            while (reader.Read())
            {
                DTO_Atividade dt = new DTO_Atividade();
                dt.ID_Atividade = reader.GetInt32("id_atividade");
                dt.Atividade = reader.GetString("nm_atividade");
                dt.Equipamento = reader.GetString("ds_equipamento");
                dt.Professor = reader.GetString("nm_professor");
                dtos.Add(dt);
            }
            reader.Close();
            return dtos;

        }

        public bool VerificarRegistro(DTO_Atividade dto)
        {
            string script = @"SELECT * FROM tb_atividade where nm_atividade = @nm_atividade";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_atividade", dto.Atividade));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if(reader.Read())
            {
                return true;
            }

            else
            {
                return false;
            }
        }
    }
}
