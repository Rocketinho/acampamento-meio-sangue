﻿using Acampamento_Meio_Sangue.DB._Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.Semideus
{
    class Database_Semideus
    {
        public int Salvar(DTO_Semideus dto)
        {
            string script = 
            @"INSERT INTO tb_semideus
            (
                nm_semideus, 
                ds_arma_primaria, 
                ds_arma_secundaria, 
                dt_nascimento, 
                ds_feitos, 
                ds_foto,
                id_chale,
                id_deus
            )
            VALUES 
            (
                @nm_semideus, 
                @ds_arma_primaria, 
                @ds_arma_secundaria, 
                @dt_nascimento, 
                @ds_feitos, 
                @ds_foto,
                @id_chale,
                @id_deus

            )";

         List<MySqlParameter> parms = new List<MySqlParameter>();
         parms.Add(new MySqlParameter("nm_semideus", dto.Nome));
         parms.Add(new MySqlParameter("ds_arma_primaria", dto.Primaria));
         parms.Add(new MySqlParameter("ds_arma_secundaria", dto.Secundaria));
         parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
         parms.Add(new MySqlParameter("ds_feitos", dto.Descricao));
         parms.Add(new MySqlParameter("ds_foto", dto.Imagem));
         parms.Add(new MySqlParameter("id_deus", dto.ID_Deus));
         parms.Add(new MySqlParameter("id_chale", dto.ID_Chale));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


    public void Alterar(DTO_Semideus dto)
    {
        string script = @"UPDATE tb_semideus SET  nm_semideus = @nm_semideus,
                                                  id_chale    = @id_chale,
                                                      ds_arma_primaria = @ds_arma_primaria,
                                                      ds_arma_secundaria = @ds_arma_secundaria,
                                                      dt_nascimento = @dt_nascimento,
                                                      ds_feitos = @ds_feitos,
                                                      ds_foto = @ds_foto
                            WHERE id_semideus = @id_semideus";

        List<MySqlParameter> parms = new List<MySqlParameter>();
        parms.Add(new MySqlParameter("id_semideus", dto.ID));
        parms.Add(new MySqlParameter("nm_semideus", dto.Nome));
        parms.Add(new MySqlParameter("id_chale", dto.ID_Chale));
        parms.Add(new MySqlParameter("ds_arma_primaria", dto.Primaria));
        parms.Add(new MySqlParameter("ds_arma_secundaria", dto.Secundaria));
        parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
        parms.Add(new MySqlParameter("ds_feitos", dto.Descricao));

            
                parms.Add(new MySqlParameter("ds_foto", dto.Imagem));

            

            Database db = new Database();
        db.ExecuteInsertScript(script, parms);
    }


    public void Remover(string nome)
    {
        string script = @"DELETE from tb_semideus
                                WHERE nm_semideus = @nm_semideus";

        List<MySqlParameter> parms = new List<MySqlParameter>();
        parms.Add(new MySqlParameter("nm_semideus", nome));

        Database db = new Database();
        db.ExecuteInsertScript(script, parms);

    }


        
        public List<DTO_Semideus> ConsultarTodosNomes()
        {
            string script = @"SELECT * FROM tb_semideus";

            
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<DTO_Semideus> lista = new List<DTO_Semideus>();
            while (reader.Read())
            {
                DTO_Semideus dto = new DTO_Semideus();
                dto.Nome = reader.GetString("nm_semideus");
                lista.Add(dto);
            }


            reader.Close();
            return lista;


        }
        public bool VerificarRegistro(DTO_Semideus dto)
        {
            string script = @"select * from tb_semideus
                            where nm_semideus = @nm_semideus";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_semideus", dto.Nome));

            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if (reader.Read())
            {
                return true;
            }

            else
            {
                return false;
            }
        }
    }
}
