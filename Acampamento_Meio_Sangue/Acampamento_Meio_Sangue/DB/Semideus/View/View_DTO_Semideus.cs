﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.DB.Semideus.View
{
  public class View_DTO_Semideus
    {
        public int ID { get; set; }

        public int ID_Chale { get; set; }

        public int ID_Deus { get; set; }

        public string Nome { get; set; }

        public string Primaria { get; set; }

        public string Secundaria { get; set; }

        public DateTime Nascimento { get; set; }

        public string Descricao { get; set; }

        public string Imagem { get; set; }

        public string Deus { get; set; }
    }
}
