﻿using Acampamento_Meio_Sangue.DB._Base;
using Acampamento_Meio_Sangue.DB.Semideus.View;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.DB.Semideus
{
    class View_Database_Semideus
    {
        public View_DTO_Semideus Consultar(View_DTO_Semideus dto)
        {
            string script = @"SELECT * FROM view_consultar_semideus WHERE nm_semideus = @nm_semideus";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_semideus", dto.Nome));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            View_DTO_Semideus dt = new View_DTO_Semideus();

            // dando erro

            if (reader.Read())
            {

                View_DTO_Semideus d = new View_DTO_Semideus();

                d.ID = reader.GetInt32("id_semideus");
                d.ID_Chale = reader.GetInt32("id_chale");
                d.ID_Deus= reader.GetInt32("id_deus");
                d.Nome = reader.GetString("nm_semideus");
                d.Primaria = reader.GetString("ds_arma_primaria");
                d.Secundaria = reader.GetString("ds_arma_secundaria");
                d.Nascimento = reader.GetDateTime("dt_nascimento");
                d.Descricao = reader.GetString("ds_feitos");
               
                
                d.Imagem = reader.GetString("ds_foto");

                
                d.Deus = reader.GetString("nm_deus");
                dt = d;

            }
     
            reader.Close();
            return dt;

        }

        public List<View_DTO_Semideus> ConsultarFilho(string Deus)
        {
            string script =
                @"SELECT * FROM view_consultar_semideus WHERE nm_deus = @nm_deus";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_deus", Deus));
         

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);


            List<View_DTO_Semideus> lista = new List<View_DTO_Semideus>();
            while (reader.Read())
            {
                View_DTO_Semideus dt = new View_DTO_Semideus();
                dt.ID = reader.GetInt32("id_semideus");                
                dt.Nome = reader.GetString("nm_semideus");
                dt.ID_Deus = reader.GetInt32("id_deus");

                lista.Add(dt);
            }

            reader.Close();
            return lista;

        }
        public bool VerificarRegistro(View_DTO_Semideus dto)
        {
            string script = @"SELECT * FROM view_consultar_semideus where id_semideus = @id_semideus";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_semideus", dto.Nome));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
