﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.DB.Semideus.View
{
    class View_Business_Semideus
    {
        public List<View_DTO_Semideus> ConsultarFilho(string Deus)
        {
            View_Database_Semideus database = new View_Database_Semideus();
            return database.ConsultarFilho(Deus);
        }
        public View_DTO_Semideus Consultar(View_DTO_Semideus dto)
        {
            View_Database_Semideus database = new View_Database_Semideus();
            return database.Consultar(dto);
        }
        public bool VerificarRegistro(View_DTO_Semideus dto)
        {
            View_Database_Semideus database = new View_Database_Semideus();
            return database.VerificarRegistro(dto);
        }
    } }
