﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.Semideus
{
    class Business_Semideus
    {
        public int Salvar(DTO_Semideus dto)
        {
            Database_Semideus db = new Database_Semideus();
            //Consultar nome do Semideuse verificar se o nome do mesmo ja existe no banco
            if (dto.Imagem == null)
            {
                throw new ArgumentException("Preencha o campo de foto");
            }

            bool existe = db.VerificarRegistro(dto);
            if (existe == false)
            {
                return db.Salvar(dto);

            }
            else
            {
                throw new ArgumentException("Esse semideus já está registrado");


                
            }
        }

        public void Alterar(DTO_Semideus dto)
        {
            Database_Semideus db = new Database_Semideus();
            db.Alterar(dto);
        }

        public void Remover(string nome)
        {
            Database_Semideus db = new Database_Semideus();
            db.Remover(nome);
        }

        public List<DTO_Semideus> ConsultarTodosNomes()
        {
            Database_Semideus db = new Database_Semideus();
            return db.ConsultarTodosNomes();
        }
        public bool VerificarRegistro(DTO_Semideus dto)
        {
            Database_Semideus db = new Database_Semideus();
            return db.VerificarRegistro(dto);
        }
    }
}
