﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.DB.Login
{
    class DTO_Usuario
    {
        public int ID { get; set; }
        public int ID_Deus { get; set; }
        public string Usuario { get; set; }
        public string Senha { get; set; }

        public bool PermissaoAdm { get; set; }
        public bool PermissaoConsultar { get; set; }

    }
}
