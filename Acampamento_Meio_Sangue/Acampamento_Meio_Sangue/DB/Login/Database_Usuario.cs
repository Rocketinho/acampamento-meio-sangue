﻿using Acampamento_Meio_Sangue.DB._Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.DB.Login
{
    class Database_Usuario
    {
        public int Salvar (DTO_Usuario dto)
        {
            string script =
            @"INSERT INTO tb_usuario
            (
                id_deus,
                nm_usuario,
                ds_senha,
                bt_permissao_adm,
                bt_permissao_consultar
            )
            VALUES
            (
                @id_deus,
                @nm_usuario,
                @ds_senha,
                @bt_permissao_adm,
                @bt_permissao_consultar
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_deus", dto.ID_Deus));
            parm.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parm.Add(new MySqlParameter("ds_senha", dto.Senha));
            parm.Add(new MySqlParameter("bt_permissao_adm", dto.PermissaoAdm));
            parm.Add(new MySqlParameter("bt_permissao_consultar", dto.PermissaoConsultar));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }


        // public bool Logar(DTO_Usuario dto)
        // {
        //     string script =
        //     @"SELECT * FROM tb_usuario
        //                WHERE ds_senha      = @ds_senha";
        // 
        //     List<MySqlParameter> parm = new List<MySqlParameter>();
        //     parm.Add(new MySqlParameter("nm_usuario", dto.Usuario));
        //     parm.Add(new MySqlParameter("ds_senha", dto.Senha));
        //     parm.Add(new MySqlParameter("id_deus", dto.ID_Deus));
        // 
        //     Database db = new Database();
        //     MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
        // 
        //     if (reader.Read())
        //     {
        //         return true;
        //     }
        //     else
        //     {
        //         return false;
        //     }
        // 
        //     
        // }
        public DTO_Usuario Log (DTO_Usuario dto)
        {
            string script =
            @"SELECT * FROM tb_usuario
                WHERE ds_senha  = @ds_senha
                  AND id_deus   = @id_deus";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_deus", dto.ID_Deus));
            parm.Add(new MySqlParameter("ds_senha", dto.Senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            DTO_Usuario usuario = null;

            if (reader.Read())
            {
                usuario = new DTO_Usuario();
                usuario.ID = reader.GetInt32("id_usuario");
                usuario.ID_Deus = reader.GetInt32("id_deus");
                usuario.Usuario = reader.GetString("nm_usuario");

                usuario.PermissaoAdm = reader.GetBoolean("bt_permissao_adm");
                usuario.PermissaoConsultar = reader.GetBoolean("bt_permissao_consultar");
            }
            reader.Close();
            return usuario;

        }

        public bool VerificarRegistro(DTO_Usuario dto)
        {
            string script = @"SELECT * FROM tb_usuario where nm_usuario = @nm_usuario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", dto.Usuario));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if(reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
