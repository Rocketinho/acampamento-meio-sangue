﻿using Acampamento_Meio_Sangue.DB._Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.DB.Chale.View
{
    class Database_View_Chale
    {
        public List<DTO_View_Chale> Consultar(DTO_View_Chale dto)
        {
            string script =
                @"SELECT * FROM view_consultar_chale WHERE nm_deus = @nm_deus";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_deus", dto.Deus));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_View_Chale> fora = new List<DTO_View_Chale>();

            while (reader.Read())
            {
                DTO_View_Chale dentro = new DTO_View_Chale();
                dentro.ID_Cronograma = reader.GetInt32("id_cronograma");
                dentro.ID = reader.GetInt32("id_chale");
                dentro.Representante = reader.GetString("nm_representante");
                dentro.Atividade = reader.GetString("nm_atividade");
                dentro.Descricao = reader.GetString("ds_chale");
                dentro.Dia = reader.GetString("dt_data");
                dentro.Horario = reader.GetString("dt_horario");
                dentro.Deus = reader.GetString("nm_deus");


                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }

        public DTO_View_Chale Listar(DTO_View_Chale dto)
        {
            string script = @"SELECT * FROM view_consultar_chale WHERE nm_deus = @nm_deus";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_deus", dto.Deus));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            DTO_View_Chale fora = new DTO_View_Chale();

            while (reader.Read())
            {
                DTO_View_Chale dentro = new DTO_View_Chale();               
                dentro.Descricao = reader.GetString("ds_chale");
                dentro.Representante = reader.GetString("nm_representante");


                fora = dentro;
            }
            reader.Close();
            return fora;
            
        }
    }
}
