﻿using Acampamento_Meio_Sangue.DB._Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.DB.Chale
{
    class DTO_View_Chale
    {
        public int ID { get; set; }

        public int ID_Cronograma { get; set; }

        public string Representante { get; set; }

        public string Descricao { get; set; }

        public string Atividade { get; set; }

        public string Dia { get; set; }

        public string Horario { get; set; }

        public string Deus { get; set; }

    }

    
}
