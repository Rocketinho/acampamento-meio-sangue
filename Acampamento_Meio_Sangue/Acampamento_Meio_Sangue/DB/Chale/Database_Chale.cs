﻿using Acampamento_Meio_Sangue.DB._Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.Chale
{
    class Database_Chale
    {
       
        public List<DTO_Chale> Listar()
        {
            string script = @"SELECT * FROM tb_chale";

            List<MySqlParameter> parm = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Chale> fora = new List<DTO_Chale>();

            while (reader.Read())
            {
                DTO_Chale dentro = new DTO_Chale();
                dentro.ID_Chale = reader.GetInt32("id_chale");
                dentro.Descricao = reader.GetString("ds_chale");
                dentro.Representante = reader.GetString("nm_representante");
               

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }

        public List<DTO_Chale> Consultar(int id)
        {
            string script = @"SELECT * FROM tb_chale where id_deus = @id_deus";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_deus", id));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Chale> fora = new List<DTO_Chale>();

            while (reader.Read())
            {
                DTO_Chale dentro = new DTO_Chale();
                dentro.ID_Chale = reader.GetInt32("id_chale");
                dentro.Descricao = reader.GetString("ds_chale");
                dentro.Representante = reader.GetString("nm_representante");


                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }

        public int Salvar(DTO_Chale dto)
        {
            string script =
            @"INSERT INTO tb_chale
            (
                id_deus
                ds_chale, 
                nm_representante,               
            )
            VALUES
            (
                @id_deus,
                @ds_chale, 
                @nm_representante,                
            )";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_chale", dto.Descricao));
            parm.Add(new MySqlParameter("id_deus", dto.ID_Deus));
            parm.Add(new MySqlParameter("nm_representante", dto.Representante));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

    }
}
