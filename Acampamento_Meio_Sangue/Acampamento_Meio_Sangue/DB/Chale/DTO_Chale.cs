﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.Chale
{
    class DTO_Chale
    {
        public int ID_Chale { get; set; }

        public int ID_Deus { get; set; }

        public string Descricao { get; set; }

        public string Representante { get; set; }
    }
}
