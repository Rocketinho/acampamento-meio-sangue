﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.Cronograma
{
    class DTO_Cronograma
    {
        public int ID_Cronograma { get; set; }

        public int ID_Chale { get; set; }

        public int ID_Atividade { get; set; }

        public string Dia { get; set; }

        public string Horario { get; set; }

    }
}
