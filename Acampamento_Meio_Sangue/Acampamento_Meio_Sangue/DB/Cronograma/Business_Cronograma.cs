﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.Cronograma
{
    class Business_Cronograma
    {
        public int Salvar(DTO_Cronograma dto)
        {
            Database_Cronograma database = new Database_Cronograma();
            return database.Salvar(dto);
        }

        public void Alterar(DTO_Cronograma dto)
        {
            Database_Cronograma database = new Database_Cronograma();
            database.Alterar(dto);
            
        }

        public void Remover(int id)
        {
            Database_Cronograma database = new Database_Cronograma();
            database.Remover(id);
        }

        public List<DTO_Cronograma> Consultar(DTO_Cronograma dto)
        {
            Database_Cronograma database = new Database_Cronograma();
            return database.Listar(dto);
        }

        public bool VerificarRegistro(DTO_Cronograma dto)
        {
            Database_Cronograma db = new Database_Cronograma();
            return db.VerificarRegistro(dto);
        }
    }
}
