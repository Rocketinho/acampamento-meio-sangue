﻿using Acampamento_Meio_Sangue.DB._Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acampamento_Meio_Sangue.Cronograma
{
    class Database_Cronograma
    {

        public int Salvar(DTO_Cronograma dto)
        {
            string script =
            @"INSERT INTO tb_cronograma
            (
                dt_data, 
                dt_horario,
                id_atividade,
                id_chale
            )
            VALUES
            (
                @dt_data, 
                @dt_horario,
                @id_atividade,
                @id_chale
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_data", dto.Dia));
            parms.Add(new MySqlParameter("dt_horario", dto.Horario));
            parms.Add(new MySqlParameter("id_atividade", dto.ID_Atividade));
            parms.Add(new MySqlParameter("id_chale", dto.ID_Chale));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Alterar(DTO_Cronograma dto)
        {
            string script = @"UPDATE tb_cronograma SET id_chale = @id_chale,
                                                        id_atividade = @id_atividade,
                                                        ds_data = @ds_data,
                                                        ds_horario = @ds_horario
            WHERE id_cronograma = @id_cronograma";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_chale", dto.ID_Chale));
            parms.Add(new MySqlParameter("id_ativividade", dto.ID_Atividade));
            parms.Add(new MySqlParameter("ds_data", dto.Dia));
            parms.Add(new MySqlParameter("ds_horario", dto.Horario));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        
        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_cronograma WHERE id_cronograma = @id_cronograma";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cronograma", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public List<DTO_Cronograma> Listar(DTO_Cronograma dto)
        {
            string script = @"SELECT * FROM tb_cronograma WHERE id_cronograma = @id_cronograma";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cronograma", dto.ID_Cronograma));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Cronograma> cronograma = new List<DTO_Cronograma>();
            while (reader.Read())
            {
                DTO_Cronograma dt = new DTO_Cronograma();
                dt.ID_Cronograma = reader.GetInt32("id_cronograma");
                dt.ID_Chale = reader.GetInt32("id_chale");
                dt.ID_Atividade = reader.GetInt32("id_atividade");
                dt.Dia = reader.GetString("ds_data");
                dt.Horario = reader.GetString("ds_horario");

                cronograma.Add(dt);
            }

            reader.Close();
            return cronograma;


        }

        public bool VerificarRegistro(DTO_Cronograma dto)
        {
            string script = @"select * from tb_cronograma where id_chale = @id_chale and 
                            dt_data = @dt_data and dt_horario = @dt_horario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_chale", dto.ID_Chale));
            parms.Add(new MySqlParameter("dt_data", dto.Dia));
            parms.Add(new MySqlParameter("dt_horario", dto.Horario));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if(reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
            


        }
    }
}
