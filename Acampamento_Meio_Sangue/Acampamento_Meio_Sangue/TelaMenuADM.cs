﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acampamento_Meio_Sangue
{
    public partial class TelaMenu : Form
    {
        public TelaMenu()
        {
            InitializeComponent();
            VerificarPermissoes();
        }
        public void VerificarPermissoes ()
        {
            if (UserSession.UsuarioLogado.PermissaoAdm == false)
            {
                if (UserSession.UsuarioLogado.PermissaoConsultar == true)
                {
                    cadrastrosToolStripMenuItem.Enabled = false;
                }
            }
        }

        //private void TelaMenu_Load(object sender, EventArgs e)
        //{
        //    if (frmLogin.NivelAcesso == "Semi-Deus")
        //    {
        //        mnStrip.Enabled = false;
        //        MessageBox.Show("Você não tem privilégios para mexer nessa parte do programa",
        //                        "Acampamento Meio-Sangue",
        //                        MessageBoxButtons.OK,
        //                        MessageBoxIcon.Information);
        //    }
        //}

        private void contaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrar_Contas tela = new frmCadastrar_Contas();
            tela.Show();
            this.Hide();
        }

        private void atividadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrar_Atividades atividades = new frmCadastrar_Atividades();
            atividades.Show();
            this.Hide();

            
        }

        private void cronogramaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmCadastrarCronograma cronograma = new frmCadastrarCronograma();
            cronograma.Show();
            this.Hide();

        }

        private void semiDeusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrar_Deus deus = new frmCadastrar_Deus();
            deus.Show();
            this.Hide();
        }

        private void semiDeusToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmCadastro_Semideus semideus = new frmCadastro_Semideus();
            semideus.Show();
            this.Hide();
        }

        private void cronogramaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultar_Semideuses semideus = new frmConsultar_Semideuses();
            semideus.Show();
            this.Hide();

        }

        private void chaléToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultar_Chale chale = new frmConsultar_Chale();
            chale.Show();
            this.Hide();

        }

        private void deusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultar_Deus deus = new frmConsultar_Deus();
            deus.Show();
            this.Hide();
        }

        private void lblX_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja sair do Acampamento?",
                                                  "Acapamento Meio-sangue",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                Application.Exit();

            }
        }

        private void semideusToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmConsultar_Semideuses tela = new frmConsultar_Semideuses();
            tela.Show();
            this.Hide();
        }

        private void contaToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmCadastrar_Contas tela = new frmCadastrar_Contas();
            tela.Show();
            this.Hide();
        }
    }
}
