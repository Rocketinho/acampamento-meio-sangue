﻿using Acampamento_Meio_Sangue.Atividade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acampamento_Meio_Sangue
{
    public partial class frmCadastrar_Atividades : Form
    {
        public frmCadastrar_Atividades()
        {
            InitializeComponent();
            txtAtividade.MaxLength = 50;
            txtEquipamento.MaxLength = 50;
            txtProfessor.MaxLength = 50;
        }

        private void lblX_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja sair do Acampamento?",
                                                  "Acapamento Meio-sangue",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                TelaMenu tela = new TelaMenu();
                tela.Show();
                this.Close();
            }
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {

              DTO_Atividade dto = new DTO_Atividade();
                if (txtAtividade.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("O campo atividade não pode estar vazio");
                }
                else
                {


                    dto.Professor = txtProfessor.Text.Trim();
                    dto.Atividade = txtAtividade.Text.Trim();
                    dto.Equipamento = txtEquipamento.Text.Trim();

                    Business_Atividade business = new Business_Atividade();
                    bool ex = business.VerificarRegistro(dto);
                    if (ex == true)
                    {
                        MessageBox.Show("Essa atividade já está registrada");
                    }
                    else
                    {

                        business.Salvar(dto);
                        MessageBox.Show("Atividade Salva com Sucesso", "Atividade - Acampamento Meio-sangue", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

            }
            catch (Exception)
            {

                MessageBox.Show("Errou");
            }


        }

        private void CadastrarAtividades_Load(object sender, EventArgs e)
        {
            if (frmLogin.NivelAcesso == "Semi-Deus")
            {
                btnCadastrar.Enabled = false;
                txtAtividade.Enabled = false;
                txtProfessor.Enabled = false;
                txtEquipamento.Enabled = false;
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            TelaMenu menu = new TelaMenu();
            menu.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DTO_Atividade dto = new DTO_Atividade();
            dto.Atividade = txtAtividade.Text.Trim();
            Business_Atividade busi = new Business_Atividade();
            bool ex = busi.VerificarRegistro(dto);
            if (ex == false)
            {
                MessageBox.Show("Registro não encontrado",
                                "Acampamento ameio-Sangue",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            else
            {



                DialogResult dialog = MessageBox.Show("Deseja mesmo apagar esse registro?",
                                                        "Acampamento Meio-Sangue",
                                                        MessageBoxButtons.YesNo,
                                                        MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    Business_Atividade bsi = new Business_Atividade();
                    bsi.Remover(txtAtividade.Text.Trim());

                    MessageBox.Show("Atividade removida com sucesso",
                                    "Acampamento Meio-Sangue",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
            }
        }

        private void txtAtividade_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {

                
                DTO_Atividade dto = new DTO_Atividade();
                if (txtAtividade.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("O campo atividade não pode estar vazio", "Atividade - Acampamento Meio-sangue", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    dto.Professor = txtProfessor.Text.Trim();
                    dto.Atividade = txtAtividade.Text.Trim();
                    dto.Equipamento = txtEquipamento.Text.Trim();

                    Business_Atividade business = new Business_Atividade();
                    bool ex = business.VerificarRegistro(dto);
                    if (ex == true)
                    {
                        MessageBox.Show("Essa atividade já está registrada");
                    }
                    else
                    {
                        business.Salvar(dto);
                        MessageBox.Show("Atividade Salva com Sucesso", "Atividade - Acampamento Meio-sangue", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                }
                catch (Exception)
                {
                    MessageBox.Show("Verifique se todos os campos estão preenchidos",
                                                       "Atividade - Acampamento Meio-sangue",
                                                       MessageBoxButtons.OK,
                                                       MessageBoxIcon.Information);
                }
               
            }
            

        }
    }
    
}
