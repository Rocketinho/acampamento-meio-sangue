﻿namespace Acampamento_Meio_Sangue
{
    partial class frmCadastrarCronograma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadastrarCronograma));
            this.lblX = new System.Windows.Forms.Label();
            this.gpbCronograma = new System.Windows.Forms.GroupBox();
            this.gpbAtribuicao = new System.Windows.Forms.GroupBox();
            this.cboAtividade = new System.Windows.Forms.ComboBox();
            this.cboChale = new System.Windows.Forms.ComboBox();
            this.lblChale = new System.Windows.Forms.Label();
            this.lblAtividade = new System.Windows.Forms.Label();
            this.gpbHorario = new System.Windows.Forms.GroupBox();
            this.txtHorario = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboSemana = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnCadastrar = new System.Windows.Forms.Button();
            this.gpbCronograma.SuspendLayout();
            this.gpbAtribuicao.SuspendLayout();
            this.gpbHorario.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.BackColor = System.Drawing.Color.Transparent;
            this.lblX.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblX.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblX.ForeColor = System.Drawing.Color.White;
            this.lblX.Location = new System.Drawing.Point(274, 1);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(25, 24);
            this.lblX.TabIndex = 13;
            this.lblX.Text = "X";
            this.lblX.Click += new System.EventHandler(this.lblX_Click);
            // 
            // gpbCronograma
            // 
            this.gpbCronograma.BackColor = System.Drawing.Color.Transparent;
            this.gpbCronograma.Controls.Add(this.gpbAtribuicao);
            this.gpbCronograma.Controls.Add(this.gpbHorario);
            this.gpbCronograma.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbCronograma.ForeColor = System.Drawing.Color.White;
            this.gpbCronograma.Location = new System.Drawing.Point(6, 22);
            this.gpbCronograma.Name = "gpbCronograma";
            this.gpbCronograma.Size = new System.Drawing.Size(297, 185);
            this.gpbCronograma.TabIndex = 22;
            this.gpbCronograma.TabStop = false;
            this.gpbCronograma.Text = "Cronograma";
            // 
            // gpbAtribuicao
            // 
            this.gpbAtribuicao.Controls.Add(this.cboAtividade);
            this.gpbAtribuicao.Controls.Add(this.cboChale);
            this.gpbAtribuicao.Controls.Add(this.lblChale);
            this.gpbAtribuicao.Controls.Add(this.lblAtividade);
            this.gpbAtribuicao.ForeColor = System.Drawing.Color.White;
            this.gpbAtribuicao.Location = new System.Drawing.Point(164, 28);
            this.gpbAtribuicao.Name = "gpbAtribuicao";
            this.gpbAtribuicao.Size = new System.Drawing.Size(128, 143);
            this.gpbAtribuicao.TabIndex = 27;
            this.gpbAtribuicao.TabStop = false;
            this.gpbAtribuicao.Text = "Atribuição";
            // 
            // cboAtividade
            // 
            this.cboAtividade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAtividade.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAtividade.FormattingEnabled = true;
            this.cboAtividade.Location = new System.Drawing.Point(6, 103);
            this.cboAtividade.Name = "cboAtividade";
            this.cboAtividade.Size = new System.Drawing.Size(115, 26);
            this.cboAtividade.TabIndex = 26;
            // 
            // cboChale
            // 
            this.cboChale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboChale.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboChale.FormattingEnabled = true;
            this.cboChale.Location = new System.Drawing.Point(6, 43);
            this.cboChale.Name = "cboChale";
            this.cboChale.Size = new System.Drawing.Size(115, 26);
            this.cboChale.TabIndex = 25;
            // 
            // lblChale
            // 
            this.lblChale.AutoSize = true;
            this.lblChale.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChale.Location = new System.Drawing.Point(6, 22);
            this.lblChale.Name = "lblChale";
            this.lblChale.Size = new System.Drawing.Size(46, 17);
            this.lblChale.TabIndex = 23;
            this.lblChale.Text = "Chalé:";
            // 
            // lblAtividade
            // 
            this.lblAtividade.AutoSize = true;
            this.lblAtividade.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtividade.Location = new System.Drawing.Point(6, 82);
            this.lblAtividade.Name = "lblAtividade";
            this.lblAtividade.Size = new System.Drawing.Size(71, 17);
            this.lblAtividade.TabIndex = 24;
            this.lblAtividade.Text = "Atividade:";
            // 
            // gpbHorario
            // 
            this.gpbHorario.Controls.Add(this.txtHorario);
            this.gpbHorario.Controls.Add(this.label2);
            this.gpbHorario.Controls.Add(this.cboSemana);
            this.gpbHorario.Controls.Add(this.label1);
            this.gpbHorario.ForeColor = System.Drawing.Color.White;
            this.gpbHorario.Location = new System.Drawing.Point(8, 28);
            this.gpbHorario.Name = "gpbHorario";
            this.gpbHorario.Size = new System.Drawing.Size(128, 143);
            this.gpbHorario.TabIndex = 25;
            this.gpbHorario.TabStop = false;
            this.gpbHorario.Text = "Horario";
            // 
            // txtHorario
            // 
            this.txtHorario.Location = new System.Drawing.Point(9, 103);
            this.txtHorario.Mask = "90:00";
            this.txtHorario.Name = "txtHorario";
            this.txtHorario.Size = new System.Drawing.Size(100, 26);
            this.txtHorario.TabIndex = 31;
            this.txtHorario.ValidatingType = typeof(System.DateTime);
            this.txtHorario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHorario_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 30;
            this.label2.Text = "Horário:";
            // 
            // cboSemana
            // 
            this.cboSemana.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSemana.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSemana.FormattingEnabled = true;
            this.cboSemana.Location = new System.Drawing.Point(9, 43);
            this.cboSemana.Name = "cboSemana";
            this.cboSemana.Size = new System.Drawing.Size(113, 26);
            this.cboSemana.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 17);
            this.label1.TabIndex = 27;
            this.label1.Text = "Dia:";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.btnVoltar);
            this.groupBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox4.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(115, 207);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(89, 46);
            this.groupBox4.TabIndex = 28;
            this.groupBox4.TabStop = false;
            // 
            // btnVoltar
            // 
            this.btnVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.Location = new System.Drawing.Point(6, 14);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(77, 26);
            this.btnVoltar.TabIndex = 10;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = true;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.btnCadastrar);
            this.groupBox5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox5.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(210, 207);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(93, 46);
            this.groupBox5.TabIndex = 27;
            this.groupBox5.TabStop = false;
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCadastrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar.Location = new System.Drawing.Point(5, 14);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(88, 26);
            this.btnCadastrar.TabIndex = 10;
            this.btnCadastrar.Text = "Cadastrar";
            this.btnCadastrar.UseVisualStyleBackColor = true;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // frmCadastrarCronograma
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackgroundImage = global::Acampamento_Meio_Sangue.Properties.Resources.Camp_half_blood;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(310, 260);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.gpbCronograma);
            this.Controls.Add(this.lblX);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCadastrarCronograma";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadrastrar Cronograma";
            this.Load += new System.EventHandler(this.frmCadastrarCronograma_Load);
            this.gpbCronograma.ResumeLayout(false);
            this.gpbAtribuicao.ResumeLayout(false);
            this.gpbAtribuicao.PerformLayout();
            this.gpbHorario.ResumeLayout(false);
            this.gpbHorario.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.GroupBox gpbCronograma;
        private System.Windows.Forms.GroupBox gpbAtribuicao;
        private System.Windows.Forms.ComboBox cboAtividade;
        private System.Windows.Forms.ComboBox cboChale;
        private System.Windows.Forms.Label lblAtividade;
        private System.Windows.Forms.Label lblChale;
        private System.Windows.Forms.GroupBox gpbHorario;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnCadastrar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboSemana;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox txtHorario;
    }
}