﻿using Acampamento_Meio_Sangue.Chale;
using Acampamento_Meio_Sangue.DB.Chale;
using Acampamento_Meio_Sangue.DB.Login;
using Acampamento_Meio_Sangue.Deus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acampamento_Meio_Sangue
{
    public partial class frmCadastrar_Contas : Form
    {
        public frmCadastrar_Contas()
        {
            InitializeComponent();
            CarregarCombo();
        }

        public void CarregarCombo ()
        {
            Business_Deus db = new Business_Deus();
            List<DTO_Deus> list = db.Listar();

            cboChales.DisplayMember = nameof(DTO_Deus.Deus);
            cboChales.DataSource = list;
        }
        private void lblX_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja saior do Acampamento?",
                                                  "Acapamento Meio-sangue",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                TelaMenu tela = new TelaMenu();
                tela.Show();
                this.Close();
            }
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            DTO_Deus dto = new DTO_Deus();
            dto.Deus = cboChales.SelectedItem.ToString();


            Business_Deus db = new Business_Deus();
            bool ex = db.VerificarRegistro(dto);

            if(ex == true)
            {
                MessageBox.Show("Essa conta já existe!",
                                "Acapamento Meio-sangue",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);
            }
            else
            {
                db.Salvar(dto);


                MessageBox.Show("Conta cadastrada com sucesso!",
                                "Acapamento Meio-sangue",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }


           
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            TelaMenu menu = new TelaMenu();
            menu.Show();
            this.Close();
        }

        private void btnCadastrar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DTO_Deus dto = new DTO_Deus();
                dto.Deus = cboChales.SelectedItem.ToString();


                Business_Deus db = new Business_Deus();
                bool ex = db.VerificarRegistro(dto);

                if (ex == true)
                {
                    MessageBox.Show("Essa conta já existe!",
                                    "Acapamento Meio-sangue",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                }
                else
                {
                    db.Salvar(dto);


                    MessageBox.Show("Conta cadastrada com sucesso!",
                                    "Acapamento Meio-sangue",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
            }
        }
    }
}
