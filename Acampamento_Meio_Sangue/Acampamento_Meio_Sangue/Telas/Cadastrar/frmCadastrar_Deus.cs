﻿using Acampamento_Meio_Sangue.ConfigurarIMGS;
using Acampamento_Meio_Sangue.Deus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acampamento_Meio_Sangue
{
    public partial class frmCadastrar_Deus : Form
    {
        public frmCadastrar_Deus()
        {
            InitializeComponent();
            txtDeus.MaxLength = 50;
            txtHabilidade.MaxLength = 300;
            rtbDescricao.MaxLength = 300;
        }
        

        private void lblX_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja sair do Acampamento?",
                                                 "Acapamento Meio-sangue",
                                                 MessageBoxButtons.YesNo,
                                                 MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                TelaMenu tela = new TelaMenu();
                tela.Show();
                this.Close();
            }
        }

        private void btnProcurar_Click(object sender, EventArgs e)
        {
           // OpenFileDialog opf = new OpenFileDialog();
           // opf.Filter = "(*.jpg; *.png)| *.jpg; *.png";
           // if(opf.ShowDialog() == DialogResult.OK)
           // {
                //imgFoto.Image = Image.FromFile(opf.FileName);              
           // }
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgFoto.ImageLocation = dialog.FileName;
            }
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtDeus.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("Nome não pode ser nulo");
                }
                else
                {
                    DTO_Deus dt = new DTO_Deus();
                    dt.Deus = txtDeus.Text.Trim();
                    dt.Descrição = rtbDescricao.Text;
                    dt.Habilidade = txtHabilidade.Text;
                    if (imgFoto.Image == null)
                    {

                    }
                    else
                    {
                        dt.Foto = ImagemPlugin.ConverterParaString(imgFoto.Image);

                    }
                    Business_Deus b = new Business_Deus();

                    bool ex = b.VerificarRegistro(dt);

                    if (ex == true)
                    {
                        MessageBox.Show("Esse deus já está registrado");
                    }


                    else
                    {
                        b.Salvar(dt);
                        MessageBox.Show("Salvo com sucesso",
                                        "Acampamento Meio-Sangue",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                    }



                }
            }
            catch (ArgumentException ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            TelaMenu menu = new TelaMenu();
            menu.Show();
            this.Close();
        }

        private void rtbDescricao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {

                    if (txtDeus.Text.Trim() == string.Empty)
                    {
                        MessageBox.Show("Nome não pode ser nulo");
                    }
                    else
                    {
                        DTO_Deus dt = new DTO_Deus();
                        dt.Deus = txtDeus.Text.Trim();
                        dt.Descrição = rtbDescricao.Text;
                        dt.Habilidade = txtHabilidade.Text;
                        if (imgFoto.Image == null)
                        {

                        }
                        else
                        {
                            dt.Foto = ImagemPlugin.ConverterParaString(imgFoto.Image);

                        }
                        Business_Deus b = new Business_Deus();

                        bool ex = b.VerificarRegistro(dt);

                        if (ex == true)
                        {
                            MessageBox.Show("Esse deus já está registrado");
                        }


                        else
                        {
                            b.Salvar(dt);
                            MessageBox.Show("Salvo com sucesso",
                                            "Acampamento Meio-Sangue",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Information);
                        }



                    }
                }
                catch (ArgumentException ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
