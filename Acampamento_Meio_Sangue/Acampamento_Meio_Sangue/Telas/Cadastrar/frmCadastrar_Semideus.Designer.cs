﻿namespace Acampamento_Meio_Sangue
{
    partial class frmCadastro_Semideus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadastro_Semideus));
            this.lblX = new System.Windows.Forms.Label();
            this.gpbDadosPessoais = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboChale = new System.Windows.Forms.ComboBox();
            this.lblNascimento = new System.Windows.Forms.Label();
            this.lblDeus = new System.Windows.Forms.Label();
            this.dtpNascimento = new System.Windows.Forms.DateTimePicker();
            this.cboDeus = new System.Windows.Forms.ComboBox();
            this.lblSegundaArma = new System.Windows.Forms.Label();
            this.txtSegundaArma = new System.Windows.Forms.TextBox();
            this.lblPrimeiraArma = new System.Windows.Forms.Label();
            this.txtPrimeiraArma = new System.Windows.Forms.TextBox();
            this.Nome = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.gpbDescricao = new System.Windows.Forms.GroupBox();
            this.rtbDescricao = new System.Windows.Forms.RichTextBox();
            this.lblDescricao = new System.Windows.Forms.Label();
            this.gpbFoto = new System.Windows.Forms.GroupBox();
            this.imgFoto = new System.Windows.Forms.PictureBox();
            this.btnProcurar = new System.Windows.Forms.Button();
            this.txtFoto = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnCadastrar = new System.Windows.Forms.Button();
            this.gpbDadosPessoais.SuspendLayout();
            this.gpbDescricao.SuspendLayout();
            this.gpbFoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFoto)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.BackColor = System.Drawing.Color.Transparent;
            this.lblX.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblX.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblX.ForeColor = System.Drawing.Color.White;
            this.lblX.Location = new System.Drawing.Point(414, 7);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(25, 24);
            this.lblX.TabIndex = 14;
            this.lblX.Text = "X";
            this.lblX.Click += new System.EventHandler(this.lblX_Click);
            // 
            // gpbDadosPessoais
            // 
            this.gpbDadosPessoais.BackColor = System.Drawing.Color.Transparent;
            this.gpbDadosPessoais.Controls.Add(this.label1);
            this.gpbDadosPessoais.Controls.Add(this.cboChale);
            this.gpbDadosPessoais.Controls.Add(this.lblNascimento);
            this.gpbDadosPessoais.Controls.Add(this.lblDeus);
            this.gpbDadosPessoais.Controls.Add(this.dtpNascimento);
            this.gpbDadosPessoais.Controls.Add(this.cboDeus);
            this.gpbDadosPessoais.Controls.Add(this.lblSegundaArma);
            this.gpbDadosPessoais.Controls.Add(this.txtSegundaArma);
            this.gpbDadosPessoais.Controls.Add(this.lblPrimeiraArma);
            this.gpbDadosPessoais.Controls.Add(this.txtPrimeiraArma);
            this.gpbDadosPessoais.Controls.Add(this.Nome);
            this.gpbDadosPessoais.Controls.Add(this.txtNome);
            this.gpbDadosPessoais.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.gpbDadosPessoais.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbDadosPessoais.ForeColor = System.Drawing.Color.White;
            this.gpbDadosPessoais.Location = new System.Drawing.Point(13, 34);
            this.gpbDadosPessoais.Name = "gpbDadosPessoais";
            this.gpbDadosPessoais.Size = new System.Drawing.Size(224, 223);
            this.gpbDadosPessoais.TabIndex = 1;
            this.gpbDadosPessoais.TabStop = false;
            this.gpbDadosPessoais.Text = "Dados Pessoais:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Chalé:";
            // 
            // cboChale
            // 
            this.cboChale.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboChale.FormattingEnabled = true;
            this.cboChale.Items.AddRange(new object[] {
            "Chalés"});
            this.cboChale.Location = new System.Drawing.Point(56, 64);
            this.cboChale.Name = "cboChale";
            this.cboChale.Size = new System.Drawing.Size(155, 24);
            this.cboChale.TabIndex = 3;
            // 
            // lblNascimento
            // 
            this.lblNascimento.AutoSize = true;
            this.lblNascimento.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNascimento.Location = new System.Drawing.Point(5, 188);
            this.lblNascimento.Name = "lblNascimento";
            this.lblNascimento.Size = new System.Drawing.Size(90, 20);
            this.lblNascimento.TabIndex = 10;
            this.lblNascimento.Text = "Nacimento:";
            // 
            // lblDeus
            // 
            this.lblDeus.AutoSize = true;
            this.lblDeus.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeus.Location = new System.Drawing.Point(6, 34);
            this.lblDeus.Name = "lblDeus";
            this.lblDeus.Size = new System.Drawing.Size(48, 20);
            this.lblDeus.TabIndex = 0;
            this.lblDeus.Text = "Deus:";
            // 
            // dtpNascimento
            // 
            this.dtpNascimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNascimento.Location = new System.Drawing.Point(101, 186);
            this.dtpNascimento.Name = "dtpNascimento";
            this.dtpNascimento.Size = new System.Drawing.Size(110, 22);
            this.dtpNascimento.TabIndex = 11;
            // 
            // cboDeus
            // 
            this.cboDeus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDeus.FormattingEnabled = true;
            this.cboDeus.Items.AddRange(new object[] {
            "Chalés"});
            this.cboDeus.Location = new System.Drawing.Point(56, 34);
            this.cboDeus.Name = "cboDeus";
            this.cboDeus.Size = new System.Drawing.Size(155, 24);
            this.cboDeus.TabIndex = 1;
            this.cboDeus.SelectedIndexChanged += new System.EventHandler(this.cboDeus_SelectedIndexChanged);
            // 
            // lblSegundaArma
            // 
            this.lblSegundaArma.AutoSize = true;
            this.lblSegundaArma.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSegundaArma.Location = new System.Drawing.Point(5, 154);
            this.lblSegundaArma.Name = "lblSegundaArma";
            this.lblSegundaArma.Size = new System.Drawing.Size(72, 20);
            this.lblSegundaArma.TabIndex = 8;
            this.lblSegundaArma.Text = "2º Arma:";
            // 
            // txtSegundaArma
            // 
            this.txtSegundaArma.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSegundaArma.Location = new System.Drawing.Point(76, 154);
            this.txtSegundaArma.Name = "txtSegundaArma";
            this.txtSegundaArma.Size = new System.Drawing.Size(135, 22);
            this.txtSegundaArma.TabIndex = 9;
            // 
            // lblPrimeiraArma
            // 
            this.lblPrimeiraArma.AutoSize = true;
            this.lblPrimeiraArma.BackColor = System.Drawing.Color.Transparent;
            this.lblPrimeiraArma.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrimeiraArma.Location = new System.Drawing.Point(4, 122);
            this.lblPrimeiraArma.Name = "lblPrimeiraArma";
            this.lblPrimeiraArma.Size = new System.Drawing.Size(72, 20);
            this.lblPrimeiraArma.TabIndex = 6;
            this.lblPrimeiraArma.Text = "1º Arma:";
            // 
            // txtPrimeiraArma
            // 
            this.txtPrimeiraArma.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrimeiraArma.Location = new System.Drawing.Point(76, 122);
            this.txtPrimeiraArma.Name = "txtPrimeiraArma";
            this.txtPrimeiraArma.Size = new System.Drawing.Size(135, 22);
            this.txtPrimeiraArma.TabIndex = 7;
            // 
            // Nome
            // 
            this.Nome.AutoSize = true;
            this.Nome.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nome.Location = new System.Drawing.Point(4, 94);
            this.Nome.Name = "Nome";
            this.Nome.Size = new System.Drawing.Size(56, 20);
            this.Nome.TabIndex = 4;
            this.Nome.Text = "Nome:";
            // 
            // txtNome
            // 
            this.txtNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.Location = new System.Drawing.Point(57, 94);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(154, 22);
            this.txtNome.TabIndex = 5;
            // 
            // gpbDescricao
            // 
            this.gpbDescricao.BackColor = System.Drawing.Color.Transparent;
            this.gpbDescricao.Controls.Add(this.rtbDescricao);
            this.gpbDescricao.Controls.Add(this.lblDescricao);
            this.gpbDescricao.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbDescricao.ForeColor = System.Drawing.Color.White;
            this.gpbDescricao.Location = new System.Drawing.Point(13, 263);
            this.gpbDescricao.Name = "gpbDescricao";
            this.gpbDescricao.Size = new System.Drawing.Size(426, 100);
            this.gpbDescricao.TabIndex = 3;
            this.gpbDescricao.TabStop = false;
            this.gpbDescricao.Text = "Descrição:";
            this.gpbDescricao.Enter += new System.EventHandler(this.gpbDescricao_Enter);
            // 
            // rtbDescricao
            // 
            this.rtbDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbDescricao.Location = new System.Drawing.Point(6, 19);
            this.rtbDescricao.Name = "rtbDescricao";
            this.rtbDescricao.Size = new System.Drawing.Size(412, 75);
            this.rtbDescricao.TabIndex = 11;
            this.rtbDescricao.Text = "";
            this.rtbDescricao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rtbDescricao_KeyDown);
            // 
            // lblDescricao
            // 
            this.lblDescricao.AutoSize = true;
            this.lblDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescricao.Location = new System.Drawing.Point(-136, -1);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.Size = new System.Drawing.Size(80, 18);
            this.lblDescricao.TabIndex = 10;
            this.lblDescricao.Text = "Descrição:";
            // 
            // gpbFoto
            // 
            this.gpbFoto.BackColor = System.Drawing.Color.Transparent;
            this.gpbFoto.Controls.Add(this.imgFoto);
            this.gpbFoto.Controls.Add(this.btnProcurar);
            this.gpbFoto.Controls.Add(this.txtFoto);
            this.gpbFoto.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbFoto.ForeColor = System.Drawing.Color.White;
            this.gpbFoto.Location = new System.Drawing.Point(255, 34);
            this.gpbFoto.Name = "gpbFoto";
            this.gpbFoto.Size = new System.Drawing.Size(184, 223);
            this.gpbFoto.TabIndex = 2;
            this.gpbFoto.TabStop = false;
            this.gpbFoto.Text = "Foto:";
            // 
            // imgFoto
            // 
            this.imgFoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgFoto.Location = new System.Drawing.Point(34, 37);
            this.imgFoto.Name = "imgFoto";
            this.imgFoto.Size = new System.Drawing.Size(106, 117);
            this.imgFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgFoto.TabIndex = 38;
            this.imgFoto.TabStop = false;
            this.imgFoto.Click += new System.EventHandler(this.imgFoto_Click);
            // 
            // btnProcurar
            // 
            this.btnProcurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProcurar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcurar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcurar.Location = new System.Drawing.Point(98, 168);
            this.btnProcurar.Name = "btnProcurar";
            this.btnProcurar.Size = new System.Drawing.Size(80, 26);
            this.btnProcurar.TabIndex = 0;
            this.btnProcurar.Text = "Procurar";
            this.btnProcurar.UseVisualStyleBackColor = true;
            this.btnProcurar.Click += new System.EventHandler(this.btnProcurar_Click);
            // 
            // txtFoto
            // 
            this.txtFoto.Enabled = false;
            this.txtFoto.Location = new System.Drawing.Point(6, 168);
            this.txtFoto.Name = "txtFoto";
            this.txtFoto.Size = new System.Drawing.Size(86, 26);
            this.txtFoto.TabIndex = 36;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.btnVoltar);
            this.groupBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(243, 363);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(89, 46);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            // 
            // btnVoltar
            // 
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.Location = new System.Drawing.Point(6, 14);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(77, 26);
            this.btnVoltar.TabIndex = 10;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = true;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.btnCadastrar);
            this.groupBox5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(338, 363);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(101, 46);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar.Location = new System.Drawing.Point(8, 14);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(87, 26);
            this.btnCadastrar.TabIndex = 10;
            this.btnCadastrar.Text = "Cadastrar";
            this.btnCadastrar.UseVisualStyleBackColor = true;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // frmCadastro_Semideus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Acampamento_Meio_Sangue.Properties.Resources.Camp_half_blood;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(450, 419);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.gpbFoto);
            this.Controls.Add(this.gpbDescricao);
            this.Controls.Add(this.gpbDadosPessoais);
            this.Controls.Add(this.lblX);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCadastro_Semideus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmCadastro_Semideus_Load);
            this.gpbDadosPessoais.ResumeLayout(false);
            this.gpbDadosPessoais.PerformLayout();
            this.gpbDescricao.ResumeLayout(false);
            this.gpbDescricao.PerformLayout();
            this.gpbFoto.ResumeLayout(false);
            this.gpbFoto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFoto)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.GroupBox gpbDadosPessoais;
        private System.Windows.Forms.Label lblNascimento;
        private System.Windows.Forms.Label lblDeus;
        private System.Windows.Forms.DateTimePicker dtpNascimento;
        private System.Windows.Forms.ComboBox cboDeus;
        private System.Windows.Forms.Label lblSegundaArma;
        private System.Windows.Forms.TextBox txtSegundaArma;
        private System.Windows.Forms.Label lblPrimeiraArma;
        private System.Windows.Forms.TextBox txtPrimeiraArma;
        private System.Windows.Forms.Label Nome;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.GroupBox gpbDescricao;
        private System.Windows.Forms.RichTextBox rtbDescricao;
        private System.Windows.Forms.Label lblDescricao;
        private System.Windows.Forms.GroupBox gpbFoto;
        private System.Windows.Forms.PictureBox imgFoto;
        private System.Windows.Forms.Button btnProcurar;
        private System.Windows.Forms.TextBox txtFoto;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnCadastrar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboChale;
    }
}