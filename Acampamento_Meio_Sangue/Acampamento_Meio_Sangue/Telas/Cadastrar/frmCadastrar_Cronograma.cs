﻿using Acampamento_Meio_Sangue.Atividade;
using Acampamento_Meio_Sangue.Chale;
using Acampamento_Meio_Sangue.Cronograma;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acampamento_Meio_Sangue
{
    public partial class frmCadastrarCronograma : Form
    {
        public frmCadastrarCronograma()
        {
            InitializeComponent();
            CarregarCombos();

            List<string> cbo = new List<string>();
            cbo.Add("Domingo");
            cbo.Add("Segunda-Feira");
            cbo.Add("Terça-Feira");
            cbo.Add("Quarta-Feira");
            cbo.Add("Quinta-Feira");
            cbo.Add("Sexta-Feira");
            cbo.Add("Sábado");

            cboSemana.DataSource = cbo;
        }


        Business_Atividade db = new Business_Atividade();
        Database_Chale data = new Database_Chale();

        public void CarregarCombos()
        {
            List<DTO_Atividade> list = db.Listar();
            List<DTO_Chale> lista = data.Listar();

            cboAtividade.ValueMember = nameof(DTO_Atividade.ID_Atividade);
            cboAtividade.DisplayMember = nameof(DTO_Atividade.Atividade);

            cboChale.ValueMember = nameof(DTO_Chale.ID_Chale);
            cboChale.DisplayMember = nameof(DTO_Chale.ID_Chale);


            cboAtividade.DataSource = list;
            cboChale.DataSource = lista;



        }

        private void lblX_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja sair do Acampamento?",
                                                  "Acapamento Meio-sangue",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                TelaMenu tela = new TelaMenu();
                tela.Show();
                this.Close();
            }
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                DTO_Chale chale = cboChale.SelectedItem as DTO_Chale;
                DTO_Atividade atividade = cboAtividade.SelectedItem as DTO_Atividade;

                DTO_Cronograma dto = new DTO_Cronograma();

                DateTime result;
                if (DateTime.TryParse(this.txtHorario.Text, out result) == false)

                {

                    MessageBox.Show("Data Inválida",
                                 "Acampamento Meio-Sangue",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Information);
                }

                else

                {
                    dto.Horario = result.ToString();
                    dto.Dia = cboSemana.SelectedItem.ToString();
                    dto.ID_Chale = chale.ID_Chale;
                    dto.ID_Atividade = atividade.ID_Atividade;

                   
                    Business_Cronograma db = new Business_Cronograma();
                    bool ex = db.VerificarRegistro(dto);
                    

                    if (ex == true)
                    {
                        MessageBox.Show("Essa data e hora já estão reservadas para este chalé",
                                        "Acampamento Meio-Sangue",
                                         MessageBoxButtons.OK,
                                         MessageBoxIcon.Information);

                    }

                    else
                    {
                        Business_Cronograma data = new Business_Cronograma();
                        data.Salvar(dto);

                        MessageBox.Show("Atividade cadastrada com sucesso!",
                                        "Acampamento Meio-Sangue",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);

                    }

                    
                }

            }
            catch (Exception)
            {

                MessageBox.Show("Verifique se os campos batem com a localidade dos Semi-deuses",
                                "Acampamento Meio-Sangue",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Information);
            }

        }


        private void btnVoltar_Click(object sender, EventArgs e)
        {
            TelaMenu menu = new TelaMenu();
            menu.Show();
            this.Close();
        }

        private void txtHorario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //try
                //{
                DTO_Chale chale = cboChale.SelectedItem as DTO_Chale;
                DTO_Atividade atividade = cboAtividade.SelectedItem as DTO_Atividade;

                DTO_Cronograma dto = new DTO_Cronograma();

                DateTime result;
                if (DateTime.TryParse(this.txtHorario.Text, out result) == false)

                {

                    MessageBox.Show("Data Inválida",
                                 "Acampamento Meio-Sangue",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Information);
                }

                else

                {
                    dto.Horario = result.ToString();
                    dto.Dia = cboSemana.SelectedItem.ToString();
                    dto.ID_Chale = chale.ID_Chale;
                    dto.ID_Atividade = atividade.ID_Atividade;


                    Business_Cronograma db = new Business_Cronograma();
                    bool ex = db.VerificarRegistro(dto);



                    if (ex == true)
                    {

                        MessageBox.Show("Essa data e hora já estão reservadas para este chalé",
                                            "Acampamento Meio-Sangue",
                                             MessageBoxButtons.OK,
                                             MessageBoxIcon.Information);


                    }
                }

                Business_Cronograma data = new Business_Cronograma();
                data.Salvar(dto);



                MessageBox.Show("Atividade cadastrada com sucesso!",
                               "Acampamento Meio-Sangue",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Information);







            }
            //}
            //catch (Exception)
            //{
            //
            //    //MessageBox.Show("Verifique se os campos batem com a localidade dos Semi-deuses",
            //    //                "Acampamento Meio-Sangue",
            //    //                 MessageBoxButtons.OK,
            //    //                 MessageBoxIcon.Information);
            //}
        }
    

        private void frmCadastrarCronograma_Load(object sender, EventArgs e)
        {

        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }
    }
}
