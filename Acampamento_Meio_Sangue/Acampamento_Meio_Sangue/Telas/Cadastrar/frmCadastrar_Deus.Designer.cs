﻿namespace Acampamento_Meio_Sangue
{
    partial class frmCadastrar_Deus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadastrar_Deus));
            this.lblX = new System.Windows.Forms.Label();
            this.gpbCadastroDeuses = new System.Windows.Forms.GroupBox();
            this.lblHabilidade = new System.Windows.Forms.Label();
            this.txtHabilidade = new System.Windows.Forms.TextBox();
            this.lblDeus = new System.Windows.Forms.Label();
            this.txtDeus = new System.Windows.Forms.TextBox();
            this.gpbFoto = new System.Windows.Forms.GroupBox();
            this.imgFoto = new System.Windows.Forms.PictureBox();
            this.btnProcurar = new System.Windows.Forms.Button();
            this.txtFoto = new System.Windows.Forms.TextBox();
            this.gpbDescricao = new System.Windows.Forms.GroupBox();
            this.rtbDescricao = new System.Windows.Forms.RichTextBox();
            this.lblDescricao = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnCadastrar = new System.Windows.Forms.Button();
            this.gpbCadastroDeuses.SuspendLayout();
            this.gpbFoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFoto)).BeginInit();
            this.gpbDescricao.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.BackColor = System.Drawing.Color.Transparent;
            this.lblX.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblX.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblX.ForeColor = System.Drawing.Color.White;
            this.lblX.Location = new System.Drawing.Point(394, 4);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(25, 24);
            this.lblX.TabIndex = 27;
            this.lblX.Text = "X";
            this.lblX.Click += new System.EventHandler(this.lblX_Click);
            // 
            // gpbCadastroDeuses
            // 
            this.gpbCadastroDeuses.BackColor = System.Drawing.Color.Transparent;
            this.gpbCadastroDeuses.Controls.Add(this.lblHabilidade);
            this.gpbCadastroDeuses.Controls.Add(this.txtHabilidade);
            this.gpbCadastroDeuses.Controls.Add(this.lblDeus);
            this.gpbCadastroDeuses.Controls.Add(this.txtDeus);
            this.gpbCadastroDeuses.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbCadastroDeuses.ForeColor = System.Drawing.Color.White;
            this.gpbCadastroDeuses.Location = new System.Drawing.Point(12, 28);
            this.gpbCadastroDeuses.Name = "gpbCadastroDeuses";
            this.gpbCadastroDeuses.Size = new System.Drawing.Size(210, 178);
            this.gpbCadastroDeuses.TabIndex = 37;
            this.gpbCadastroDeuses.TabStop = false;
            this.gpbCadastroDeuses.Text = "Cadastro de Deuses";
            // 
            // lblHabilidade
            // 
            this.lblHabilidade.AutoSize = true;
            this.lblHabilidade.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHabilidade.Location = new System.Drawing.Point(6, 99);
            this.lblHabilidade.Name = "lblHabilidade";
            this.lblHabilidade.Size = new System.Drawing.Size(82, 19);
            this.lblHabilidade.TabIndex = 23;
            this.lblHabilidade.Text = "Habilidade:";
            this.lblHabilidade.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtHabilidade
            // 
            this.txtHabilidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHabilidade.Location = new System.Drawing.Point(10, 120);
            this.txtHabilidade.Name = "txtHabilidade";
            this.txtHabilidade.Size = new System.Drawing.Size(137, 22);
            this.txtHabilidade.TabIndex = 22;
            // 
            // lblDeus
            // 
            this.lblDeus.AutoSize = true;
            this.lblDeus.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeus.Location = new System.Drawing.Point(7, 37);
            this.lblDeus.Name = "lblDeus";
            this.lblDeus.Size = new System.Drawing.Size(44, 19);
            this.lblDeus.TabIndex = 21;
            this.lblDeus.Text = "Deus:";
            // 
            // txtDeus
            // 
            this.txtDeus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDeus.Location = new System.Drawing.Point(10, 58);
            this.txtDeus.Name = "txtDeus";
            this.txtDeus.Size = new System.Drawing.Size(137, 22);
            this.txtDeus.TabIndex = 20;
            // 
            // gpbFoto
            // 
            this.gpbFoto.BackColor = System.Drawing.Color.Transparent;
            this.gpbFoto.Controls.Add(this.imgFoto);
            this.gpbFoto.Controls.Add(this.btnProcurar);
            this.gpbFoto.Controls.Add(this.txtFoto);
            this.gpbFoto.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbFoto.ForeColor = System.Drawing.Color.White;
            this.gpbFoto.Location = new System.Drawing.Point(238, 28);
            this.gpbFoto.Name = "gpbFoto";
            this.gpbFoto.Size = new System.Drawing.Size(181, 178);
            this.gpbFoto.TabIndex = 39;
            this.gpbFoto.TabStop = false;
            this.gpbFoto.Text = "Foto:";
            // 
            // imgFoto
            // 
            this.imgFoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgFoto.Location = new System.Drawing.Point(34, 37);
            this.imgFoto.Name = "imgFoto";
            this.imgFoto.Size = new System.Drawing.Size(106, 97);
            this.imgFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgFoto.TabIndex = 38;
            this.imgFoto.TabStop = false;
            // 
            // btnProcurar
            // 
            this.btnProcurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProcurar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcurar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcurar.Location = new System.Drawing.Point(98, 140);
            this.btnProcurar.Name = "btnProcurar";
            this.btnProcurar.Size = new System.Drawing.Size(75, 29);
            this.btnProcurar.TabIndex = 37;
            this.btnProcurar.Text = "Procurar";
            this.btnProcurar.UseVisualStyleBackColor = true;
            this.btnProcurar.Click += new System.EventHandler(this.btnProcurar_Click);
            // 
            // txtFoto
            // 
            this.txtFoto.Enabled = false;
            this.txtFoto.Location = new System.Drawing.Point(6, 140);
            this.txtFoto.Name = "txtFoto";
            this.txtFoto.Size = new System.Drawing.Size(86, 28);
            this.txtFoto.TabIndex = 36;
            // 
            // gpbDescricao
            // 
            this.gpbDescricao.BackColor = System.Drawing.Color.Transparent;
            this.gpbDescricao.Controls.Add(this.rtbDescricao);
            this.gpbDescricao.Controls.Add(this.lblDescricao);
            this.gpbDescricao.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbDescricao.ForeColor = System.Drawing.Color.White;
            this.gpbDescricao.Location = new System.Drawing.Point(12, 212);
            this.gpbDescricao.Name = "gpbDescricao";
            this.gpbDescricao.Size = new System.Drawing.Size(407, 100);
            this.gpbDescricao.TabIndex = 41;
            this.gpbDescricao.TabStop = false;
            this.gpbDescricao.Text = "Descrição:";
            // 
            // rtbDescricao
            // 
            this.rtbDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbDescricao.Location = new System.Drawing.Point(6, 19);
            this.rtbDescricao.Name = "rtbDescricao";
            this.rtbDescricao.Size = new System.Drawing.Size(395, 75);
            this.rtbDescricao.TabIndex = 11;
            this.rtbDescricao.Text = "";
            this.rtbDescricao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rtbDescricao_KeyDown);
            // 
            // lblDescricao
            // 
            this.lblDescricao.AutoSize = true;
            this.lblDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescricao.Location = new System.Drawing.Point(-136, -1);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.Size = new System.Drawing.Size(80, 18);
            this.lblDescricao.TabIndex = 10;
            this.lblDescricao.Text = "Descrição:";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.btnVoltar);
            this.groupBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox4.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(228, 312);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(89, 46);
            this.groupBox4.TabIndex = 43;
            this.groupBox4.TabStop = false;
            // 
            // btnVoltar
            // 
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.Location = new System.Drawing.Point(6, 15);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(77, 26);
            this.btnVoltar.TabIndex = 10;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = true;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.btnCadastrar);
            this.groupBox5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox5.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(323, 312);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(96, 46);
            this.groupBox5.TabIndex = 42;
            this.groupBox5.TabStop = false;
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar.Location = new System.Drawing.Point(6, 15);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(83, 26);
            this.btnCadastrar.TabIndex = 10;
            this.btnCadastrar.Text = "Cadastrar";
            this.btnCadastrar.UseVisualStyleBackColor = true;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // frmCadastrar_Deus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Acampamento_Meio_Sangue.Properties.Resources.Camp_half_blood;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(430, 367);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.gpbDescricao);
            this.Controls.Add(this.gpbFoto);
            this.Controls.Add(this.gpbCadastroDeuses);
            this.Controls.Add(this.lblX);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCadastrar_Deus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.gpbCadastroDeuses.ResumeLayout(false);
            this.gpbCadastroDeuses.PerformLayout();
            this.gpbFoto.ResumeLayout(false);
            this.gpbFoto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFoto)).EndInit();
            this.gpbDescricao.ResumeLayout(false);
            this.gpbDescricao.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.GroupBox gpbCadastroDeuses;
        private System.Windows.Forms.Label lblHabilidade;
        private System.Windows.Forms.TextBox txtHabilidade;
        private System.Windows.Forms.Label lblDeus;
        private System.Windows.Forms.TextBox txtDeus;
        private System.Windows.Forms.GroupBox gpbFoto;
        private System.Windows.Forms.PictureBox imgFoto;
        private System.Windows.Forms.Button btnProcurar;
        private System.Windows.Forms.TextBox txtFoto;
        private System.Windows.Forms.GroupBox gpbDescricao;
        private System.Windows.Forms.RichTextBox rtbDescricao;
        private System.Windows.Forms.Label lblDescricao;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnCadastrar;
    }
}