﻿using Acampamento_Meio_Sangue.Atividade;
using Acampamento_Meio_Sangue.Chale;
using Acampamento_Meio_Sangue.ConfigurarIMGS;
using Acampamento_Meio_Sangue.Deus;
using Acampamento_Meio_Sangue.Semideus;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acampamento_Meio_Sangue
{
    public partial class frmCadastro_Semideus : Form
    {
        public frmCadastro_Semideus()
        {
            InitializeComponent();
            CarregarCombos();
            txtNome.MaxLength = 50;
            txtPrimeiraArma.MaxLength = 50;
            txtSegundaArma.MaxLength = 50;
            rtbDescricao.MaxLength = 300;
        }



        Business_Deus db = new Business_Deus();
        Database_Chale data = new Database_Chale();

        public void CarregarCombos()
        {
            List<DTO_Deus> list = db.Listar();
            
            cboDeus.ValueMember = nameof(DTO_Deus.ID_Deus);
            cboDeus.DisplayMember = nameof(DTO_Deus.Deus);
            cboDeus.DataSource = list;
            cboDeus.DropDownStyle = ComboBoxStyle.DropDownList;

            CarregarChale(1);
        }

        void CarregarChale(int idDeus)
        {
            List<DTO_Chale> lista = data.Consultar(idDeus);
            cboChale.DisplayMember = nameof(DTO_Chale.ID_Chale);
            cboChale.ValueMember = nameof(DTO_Chale.ID_Deus);
            cboChale.DataSource = lista;
            cboChale.DropDownStyle = ComboBoxStyle.DropDownList;
        }


        private void lblX_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja sair do Acampamento?",
                                                  "Acapamento Meio-sangue",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                TelaMenu tela = new TelaMenu();
                tela.Show();
                this.Close();
            }
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {


                DTO_Deus deus = cboDeus.SelectedItem as DTO_Deus;
                DTO_Chale chale = cboChale.SelectedItem as DTO_Chale;



                DTO_Semideus dto = new DTO_Semideus();
                if (txtNome.Text.Trim() == string.Empty)
                {
                    MessageBox.Show("o nome não pode estar vazio");
                }

                
                else
                {
                    dto.Nome = txtNome.Text.Trim();
                    dto.Primaria = txtPrimeiraArma.Text;
                    dto.Secundaria = txtSegundaArma.Text;
                    dto.Nascimento = dtpNascimento.Value;
                    dto.Descricao = rtbDescricao.Text;



                    if (imgFoto.Image == null)
                    {
                        imgFoto.Image = null;
                    }

                    else
                    {
                        dto.Imagem = ImagemPlugin.ConverterParaString(imgFoto.Image);
                    }
                    dto.ID_Deus = deus.ID_Deus;
                    dto.ID_Chale = chale.ID_Chale;



                    //Business_Semideus db = new Business_Semideus();
                    //db.Salvar(dto);

                    Business_Semideus db = new Business_Semideus();
                    //Consultar nome do Semideuse verificar se o nome do mesmo ja existe no banco

                    bool existe = db.VerificarRegistro(dto);
                    if (existe == false)
                    {
                        db.Salvar(dto);

                        MessageBox.Show("Salvo com sucesso",
                                        "Acampamento Meio-Sangue",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);

                    }
                    else
                    {
                        MessageBox.Show("Esse semideus já está registrado");
                    }
                }




            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnProcurar_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgFoto.ImageLocation = dialog.FileName;
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            TelaMenu menu = new TelaMenu();
            menu.Show();
            this.Close();
        }

        private void rtbDescricao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {


                    DTO_Deus deus = cboDeus.SelectedItem as DTO_Deus;
                    DTO_Chale chale = cboChale.SelectedItem as DTO_Chale;



                    DTO_Semideus dto = new DTO_Semideus();
                    if (txtNome.Text.Trim() == string.Empty)
                    {
                        MessageBox.Show("o nome não pode estar vazio");
                    }


                    else
                    {
                        dto.Nome = txtNome.Text.Trim();
                        dto.Primaria = txtPrimeiraArma.Text;
                        dto.Secundaria = txtSegundaArma.Text;
                        dto.Nascimento = dtpNascimento.Value;
                        dto.Descricao = rtbDescricao.Text;



                        if (imgFoto.Image == null)
                        {
                            imgFoto.Image = null;
                        }

                        else
                        {
                            dto.Imagem = ImagemPlugin.ConverterParaString(imgFoto.Image);
                        }
                        dto.ID_Deus = deus.ID_Deus;
                        dto.ID_Chale = chale.ID_Chale;



                        //Business_Semideus db = new Business_Semideus();
                        //db.Salvar(dto);

                        Business_Semideus db = new Business_Semideus();
                        //Consultar nome do Semideuse verificar se o nome do mesmo ja existe no banco

                        bool existe = db.VerificarRegistro(dto);
                        if (existe == false)
                        {
                            db.Salvar(dto);

                            MessageBox.Show("Salvo com sucesso",
                                            "Acampamento Meio-Sangue",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Information);

                        }
                        else
                        {
                            MessageBox.Show("Esse semideus já está registrado");
                        }
                    }




                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void gpbDescricao_Enter(object sender, EventArgs e)
        {

        }

        private void frmCadastro_Semideus_Load(object sender, EventArgs e)
        {

        }

        private void imgFoto_Click(object sender, EventArgs e)
        {

        }

        private void cboDeus_SelectedIndexChanged(object sender, EventArgs e)
        {
            DTO_Deus deus = cboDeus.SelectedItem as DTO_Deus;
            CarregarChale(deus.ID_Deus);
        }
    }
}
