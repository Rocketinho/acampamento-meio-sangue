﻿using Acampamento_Meio_Sangue.Chale;
using Acampamento_Meio_Sangue.ConfigurarIMGS;
using Acampamento_Meio_Sangue.Deus;
using Acampamento_Meio_Sangue.Semideus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acampamento_Meio_Sangue.Telas.Consulta
{
    public partial class frmAlterarSemi : Form
    {
        public frmAlterarSemi()
        {
            InitializeComponent();
            CarregarCombos();
            txtNome.MaxLength = 50;
            txtPrimeiraArma.MaxLength = 50;
            txtSegundaArma.MaxLength = 50;
            rtbDescricao.MaxLength = 300;


        }
        DTO_Semideus dto = new DTO_Semideus();

        public void Loadscreen(DTO_Semideus dto)

        {
            this.dto = dto;
            txtNome.Text = dto.Nome;
            txtPrimeiraArma.Text = dto.Primaria;
            txtSegundaArma.Text = dto.Secundaria;
            dtpNascimento.Value = dto.Nascimento;
            rtbDescricao.Text = dto.Descricao;
            imgFoto.Image = ImagemPlugin.ConverterParaImagem(dto.Imagem);
        }

        void CarregarCombos()
        {
            Business_Deus db = new Business_Deus();
            Database_Chale data = new Database_Chale();
            List<DTO_Deus> list = db.Listar();
            List<DTO_Chale> lista = data.Listar();

            cboDeus.ValueMember = nameof(DTO_Deus.ID_Deus);
            cboDeus.DisplayMember = nameof(DTO_Deus.Deus);


            cboChale.DisplayMember = nameof(DTO_Chale.ID_Chale);
            cboChale.ValueMember = nameof(DTO_Chale.ID_Deus);



            cboDeus.DataSource = list;
            cboChale.DataSource = lista;
            cboChale.DropDownStyle = ComboBoxStyle.DropDownList;
            cboDeus.DropDownStyle = ComboBoxStyle.DropDownList;




        }

        void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        void btnCadastrar_Click(object sender, EventArgs e)
        {

            if (txtNome.Text.Trim() == string.Empty)
            {
                MessageBox.Show("o nome não pode estar vazio");
            }
            this.dto.Nome = txtNome.Text;
            this.dto.Primaria = txtPrimeiraArma.Text;
            this.dto.Secundaria = txtSegundaArma.Text;
            this.dto.Nascimento = Convert.ToDateTime(dtpNascimento.Text);
            this.dto.Descricao = rtbDescricao.Text;
            

           
                this.dto.Imagem = ImagemPlugin.ConverterParaString(imgFoto.Image);
            
            

            Business_Semideus busi = new Business_Semideus();


            busi.Alterar(dto);

            MessageBox.Show("Alterado com sucesso",
                            "Acampamento Meio-Sangue",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }

        void btnProcurar_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgFoto.ImageLocation = dialog.FileName;
            }
        }

        void lblX_Click(object sender, EventArgs e)
        {

            DialogResult dialog = MessageBox.Show("Tem certeza que deseja sair do Acampamento?",
                                                  "Acapamento Meio-sangue",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                TelaMenu tela = new TelaMenu();
                tela.Show();
                this.Close();
            }
        }

        private void frmAlterarSemi_Load(object sender, EventArgs e)
        {

        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmConsultar_Semideuses tela = new frmConsultar_Semideuses();
            tela.Show();
            this.Close();
        }
    }
}

