﻿using Acampamento_Meio_Sangue.Chale;
using Acampamento_Meio_Sangue.DB.Chale;
using Acampamento_Meio_Sangue.DB.Chale.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acampamento_Meio_Sangue
{
    public partial class frmConsultar_Chale : Form
    {
        public frmConsultar_Chale()
        {
            InitializeComponent();
            
            
        }

        private void lblX_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja sair do Acampamento?",
                                                  "Acapamento Meio-sangue",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                TelaMenu tela = new TelaMenu();
                tela.Show();
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TelaMenu menu = new TelaMenu();
            menu.Show();
            this.Close();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            //try
            //{
            if (txtPesquisa.Text == string.Empty)
            {
                MessageBox.Show("Preencha o campo de pesquisa", "Acapamento Meio-sangue",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DTO_View_Chale dtoView = new DTO_View_Chale();
                dtoView.Deus = txtPesquisa.Text;

                Database_View_Chale view = new Database_View_Chale();
                List<DTO_View_Chale> lista = new List<DTO_View_Chale>();

                lista = view.Consultar(dtoView);
                dgvCronograma.AutoGenerateColumns = false;
                dgvCronograma.DataSource = lista;

                //Labels
                Database_View_Chale databaseLabels = new Database_View_Chale();
                DTO_View_Chale dto = new DTO_View_Chale();
                dto = databaseLabels.Listar(dtoView);
                rtbDescricao.Text = dto.Descricao;
                lblRepresentante.Text = dto.Representante;
            }
            //}
            //catch (Exception)
            //{

               
            //MessageBox.Show("Preenchimento incorreto!",
                            //"Acampamento Meio-Sangue",
                            //MessageBoxButtons.OK,
                            //MessageBoxIcon.Information);
            //}



        }

        private void txtPesquisa_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                if (txtPesquisa.Text == string.Empty)
                {
                    MessageBox.Show("Preencha o campo de pesquisa", "Acapamento Meio-sangue",
                                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    DTO_View_Chale dtoView = new DTO_View_Chale();
                    dtoView.Deus = txtPesquisa.Text;

                    Database_View_Chale view = new Database_View_Chale();
                    List<DTO_View_Chale> lista = new List<DTO_View_Chale>();

                    lista = view.Consultar(dtoView);
                    dgvCronograma.AutoGenerateColumns = false;
                    dgvCronograma.DataSource = lista;

                    //Labels
                    Database_View_Chale databaseLabels = new Database_View_Chale();
                    DTO_View_Chale dto = new DTO_View_Chale();
                    dto = databaseLabels.Listar(dtoView);
                    rtbDescricao.Text = dto.Descricao;
                    lblRepresentante.Text = dto.Representante;
                }
                }
                catch (Exception)
                {

                    MessageBox.Show("Preenchimento incorreto!", 
                                    "Acampamento Meio-Sangue",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
            }
        }

        private void frmConsultar_Chale_Load(object sender, EventArgs e)
        {

        }

        private void dgvCronograma_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
