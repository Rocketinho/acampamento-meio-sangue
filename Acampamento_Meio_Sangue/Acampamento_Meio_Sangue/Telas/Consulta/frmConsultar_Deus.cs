﻿using Acampamento_Meio_Sangue.ConfigurarIMGS;
using Acampamento_Meio_Sangue.DB.Deus;
using Acampamento_Meio_Sangue.DB.Deus.View;
using Acampamento_Meio_Sangue.DB.Semideus.View;
using Acampamento_Meio_Sangue.Deus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acampamento_Meio_Sangue
{
    public partial class frmConsultar_Deus : Form
    {
        public frmConsultar_Deus()
        {
            InitializeComponent();
            

        }

        private void lblX_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja sair do Acampamento?",
                                                  "Acapamento Meio-sangue",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                TelaMenu tela = new TelaMenu();
                tela.Show();
                this.Close();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPesquisa.Text == string.Empty)
                {
                    MessageBox.Show("Preencha o campo de pesquisa", "Acapamento Meio-sangue",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    View_DTO_Deus pesquisa = new View_DTO_Deus();

                    View_Business_Deus business = new View_Business_Deus();

                    pesquisa.Deus = txtPesquisa.Text.Trim();

                    //Checar se ele pesquisou o Deus Certo
                    bool ex = business.VerificarRegistro(pesquisa);

                    if (ex == false)
                    {
                        MessageBox.Show("Deus não encontrado", "Acampamento Meio-Sangue",
                                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {




                        View_DTO_Deus dto = business.Consultar(pesquisa);

                        lblNomeDeus.Text = dto.Deus;
                        if (dto.Foto == "")
                        {
                            imgDeus.Image = null;
                        }
                        else
                        {
                            imgDeus.Image = ImagemPlugin.ConverterParaImagem(dto.Foto);
                        }

                        rtbHablidade.Text = dto.Habilidade;
                        rtbDescricao.Text = dto.Descrição;

                        View_Business_Semideus semideus = new View_Business_Semideus();
                        List<View_DTO_Semideus> filhos = semideus.ConsultarFilho(txtPesquisa.Text);


                        if (filhos == null)
                        {
                        }

                        else
                        {
                            dtpFilhos.AutoGenerateColumns = false;
                            dtpFilhos.DataSource = filhos;
                        }

                    }

                }
            }
            catch (ArgumentException erro)
            {

                MessageBox.Show(erro.Message, "Acampamento Meio-Sangue",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }



        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            TelaMenu menu = new TelaMenu();
            menu.Show();
            this.Close();
        }

        private void txtPesquisa_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    if (txtPesquisa.Text == string.Empty)
                    {
                        MessageBox.Show("Preencha o campo de pesquisa", "Acapamento Meio-sangue",
                                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        View_DTO_Deus pesquisa = new View_DTO_Deus();

                        View_Business_Deus business = new View_Business_Deus();

                        pesquisa.Deus = txtPesquisa.Text.Trim();

                        //Checar se ele pesquisou o Deus Certo
                        bool ex = business.VerificarRegistro(pesquisa);

                        if (ex == false)
                        {
                            MessageBox.Show("Deus não encontrado", "Acampamento Meio-Sangue",
                                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {




                            View_DTO_Deus dto = business.Consultar(pesquisa);

                            lblNomeDeus.Text = dto.Deus;
                            if (dto.Foto == "")
                            {
                                imgDeus.Image = null;
                            }
                            else
                            {
                                imgDeus.Image = ImagemPlugin.ConverterParaImagem(dto.Foto);
                            }

                            rtbHablidade.Text = dto.Habilidade;
                            rtbDescricao.Text = dto.Descrição;

                            View_Business_Semideus semideus = new View_Business_Semideus();
                            List<View_DTO_Semideus> filhos = semideus.ConsultarFilho(txtPesquisa.Text);


                            if (filhos == null)
                            {
                            }

                            else
                            {
                                dtpFilhos.AutoGenerateColumns = false;
                                dtpFilhos.DataSource = filhos;
                            }

                        }

                    }
                }
                catch (ArgumentException erro)
                {

                    MessageBox.Show(erro.Message, "Acampamento Meio-Sangue",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
            }
        }

        private void gpbConsultarDeuses_Enter(object sender, EventArgs e)
        {

        }

        private void dtpFilhos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtPesquisa_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
