﻿using Acampamento_Meio_Sangue.ConfigurarIMGS;
using Acampamento_Meio_Sangue.DB.Semideus;
using Acampamento_Meio_Sangue.DB.Semideus.View;
using Acampamento_Meio_Sangue.Semideus;
using Acampamento_Meio_Sangue.Telas.Consulta;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acampamento_Meio_Sangue
{
    public partial class frmConsultar_Semideuses : Form
    {
        public frmConsultar_Semideuses()
        {
            InitializeComponent();
            Permissao();

        }
        
        public void Permissao()
        {
            if (UserSession.UsuarioLogado.PermissaoAdm == false)
            {
                if (UserSession.UsuarioLogado.PermissaoConsultar == true)
                {
                    btnAlterar.Enabled = false;
                    btnRemover.Enabled = false;
                }
            }
        }


            private void lblX_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja sair do Acampamento?",
                                                  "Acapamento Meio-sangue",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        //void CarregarCombo()
        //{
        //    Business_Semideus business = new Business_Semideus();
        //    List<DTO_Semideus> lista = business.ConsultarTodosNomes();
        //    cboSemideus.ValueMember = nameof(DTO_Semideus.Nome);
        //    cboSemideus.DisplayMember = nameof(DTO_Semideus.Nome);


        //    cboSemideus.DataSource = lista;
        //} Realmente nao há local para carregar o combo!

        View_DTO_Semideus dto = new View_DTO_Semideus();
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {

            
            //dto.Nome = cboSemideus.SelectedItem.ToString();

            View_Business_Semideus view = new View_Business_Semideus();

            
           this.dto.Nome = txtPesquisa.Text.Trim();
               
          
           bool teste = view.VerificarRegistro(dto);
            if (teste == true)
            {
                MessageBox.Show("Esse semideus não existe", "Acampamento Meio-Sangue", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {



                dto = view.Consultar(dto);
                lblNomeSemiDeus.Text = dto.Nome;
                IDCodigo.Text = dto.ID.ToString();
                Id_chale.Text = Convert.ToString(dto.ID_Chale);
                iD_DEUS.Text = dto.ID_Deus.ToString();
                lblArma1.Text = dto.Primaria;
                lblArma2.Text = dto.Secundaria;
                lblChal3.Text = dto.Deus;
                lblDescrica0.Text = dto.Descricao;
                lblNascimento.Text = Convert.ToString(dto.Nascimento);
                imgSemideus.Image = ImagemPlugin.ConverterParaImagem(dto.Imagem);

            }
            }
            catch (Exception)
            {
                MessageBox.Show("Verifique com o Representante do chale, se o uso desta função está correta!", "Acampamento Meio-Sangue", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            TelaMenu menu = new TelaMenu();
            menu.Show();
            
            this.Close();
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Deseja mesmo deletar esse registro?",
                                                    "Acampamento Meio-Sangue",
                                                    MessageBoxButtons.YesNo,
                                                    MessageBoxIcon.Question);
            if(dialog == DialogResult.Yes)
            {
                Business_Semideus busi = new Business_Semideus();
               
                busi.Remover(txtPesquisa.Text.Trim());

                MessageBox.Show("Registro removido",
                                "Acampamento Meio-Sangue",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Information);
                
               
            }

            
        }

        private void label1_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja sair do Acampamento?",
                                                 "Acapamento Meio-sangue",
                                                 MessageBoxButtons.YesNo,
                                                 MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                TelaMenu tela = new TelaMenu();
                tela.Show();
                this.Close();
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {

            DTO_Semideus dto = new DTO_Semideus();
            dto.ID = int.Parse(IDCodigo.Text);
            dto.Nome = lblNomeSemiDeus.Text;
            dto.Primaria = lblArma1.Text;
            dto.Secundaria = lblArma2.Text;
            dto.ID_Chale = int.Parse(Id_chale.Text);
            dto.Nascimento = Convert.ToDateTime(lblNascimento.Text);
            dto.Descricao = lblDescrica0.Text;
            dto.Imagem = ImagemPlugin.ConverterParaString(imgSemideus.Image);
            dto.ID_Deus = int.Parse(iD_DEUS.Text);

            frmAlterarSemi semideus = new frmAlterarSemi();
            semideus.Loadscreen(dto);
            semideus.ShowDialog();

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void frmConsultar_Semideuses_Load(object sender, EventArgs e)
        {

        }

        private void imgSemideus_Click(object sender, EventArgs e)
        {

        }

        private void ConsultarSemiDeus_Enter(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
