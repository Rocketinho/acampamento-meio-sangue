﻿namespace Acampamento_Meio_Sangue
{
    partial class frmConsultar_Deus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsultar_Deus));
            this.lblX = new System.Windows.Forms.Label();
            this.lblFilho = new System.Windows.Forms.Label();
            this.gpbConsultarDeuses = new System.Windows.Forms.GroupBox();
            this.lblDeus = new System.Windows.Forms.Label();
            this.txtPesquisa = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.gpbDescricao = new System.Windows.Forms.GroupBox();
            this.rtbDescricao = new System.Windows.Forms.RichTextBox();
            this.lblDescricao = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.gpbNome = new System.Windows.Forms.GroupBox();
            this.lblNomeDeus = new System.Windows.Forms.Label();
            this.imgDeus = new System.Windows.Forms.PictureBox();
            this.gpbHabilidade = new System.Windows.Forms.GroupBox();
            this.rtbHablidade = new System.Windows.Forms.RichTextBox();
            this.gpbFilhos = new System.Windows.Forms.GroupBox();
            this.dtpFilhos = new System.Windows.Forms.DataGridView();
            this.Filhos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gpbConsultarDeuses.SuspendLayout();
            this.gpbDescricao.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.gpbNome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgDeus)).BeginInit();
            this.gpbHabilidade.SuspendLayout();
            this.gpbFilhos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilhos)).BeginInit();
            this.SuspendLayout();
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.BackColor = System.Drawing.Color.Transparent;
            this.lblX.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblX.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblX.ForeColor = System.Drawing.Color.White;
            this.lblX.Location = new System.Drawing.Point(435, 9);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(25, 24);
            this.lblX.TabIndex = 12;
            this.lblX.Text = "X";
            this.lblX.Click += new System.EventHandler(this.lblX_Click);
            // 
            // lblFilho
            // 
            this.lblFilho.AutoSize = true;
            this.lblFilho.BackColor = System.Drawing.Color.Transparent;
            this.lblFilho.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilho.ForeColor = System.Drawing.Color.Black;
            this.lblFilho.Location = new System.Drawing.Point(345, 103);
            this.lblFilho.Name = "lblFilho";
            this.lblFilho.Size = new System.Drawing.Size(0, 18);
            this.lblFilho.TabIndex = 25;
            // 
            // gpbConsultarDeuses
            // 
            this.gpbConsultarDeuses.BackColor = System.Drawing.Color.Transparent;
            this.gpbConsultarDeuses.Controls.Add(this.lblDeus);
            this.gpbConsultarDeuses.Controls.Add(this.txtPesquisa);
            this.gpbConsultarDeuses.Controls.Add(this.btnBuscar);
            this.gpbConsultarDeuses.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbConsultarDeuses.ForeColor = System.Drawing.Color.White;
            this.gpbConsultarDeuses.Location = new System.Drawing.Point(12, 30);
            this.gpbConsultarDeuses.Name = "gpbConsultarDeuses";
            this.gpbConsultarDeuses.Size = new System.Drawing.Size(448, 68);
            this.gpbConsultarDeuses.TabIndex = 1;
            this.gpbConsultarDeuses.TabStop = false;
            this.gpbConsultarDeuses.Text = "Consultar Deuses: ";
            this.gpbConsultarDeuses.Enter += new System.EventHandler(this.gpbConsultarDeuses_Enter);
            // 
            // lblDeus
            // 
            this.lblDeus.AutoSize = true;
            this.lblDeus.BackColor = System.Drawing.Color.Transparent;
            this.lblDeus.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeus.ForeColor = System.Drawing.Color.White;
            this.lblDeus.Location = new System.Drawing.Point(4, 32);
            this.lblDeus.Name = "lblDeus";
            this.lblDeus.Size = new System.Drawing.Size(52, 18);
            this.lblDeus.TabIndex = 10;
            this.lblDeus.Text = "Deus:";
            // 
            // txtPesquisa
            // 
            this.txtPesquisa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPesquisa.Location = new System.Drawing.Point(57, 31);
            this.txtPesquisa.Name = "txtPesquisa";
            this.txtPesquisa.Size = new System.Drawing.Size(281, 22);
            this.txtPesquisa.TabIndex = 0;
            this.txtPesquisa.TextChanged += new System.EventHandler(this.txtPesquisa_TextChanged);
            this.txtPesquisa.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPesquisa_KeyDown);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Location = new System.Drawing.Point(353, 29);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 26);
            this.btnBuscar.TabIndex = 1;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // gpbDescricao
            // 
            this.gpbDescricao.BackColor = System.Drawing.Color.Transparent;
            this.gpbDescricao.Controls.Add(this.rtbDescricao);
            this.gpbDescricao.Controls.Add(this.lblDescricao);
            this.gpbDescricao.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbDescricao.ForeColor = System.Drawing.Color.White;
            this.gpbDescricao.Location = new System.Drawing.Point(12, 384);
            this.gpbDescricao.Name = "gpbDescricao";
            this.gpbDescricao.Size = new System.Drawing.Size(448, 112);
            this.gpbDescricao.TabIndex = 5;
            this.gpbDescricao.TabStop = false;
            this.gpbDescricao.Text = "Descrição:";
            // 
            // rtbDescricao
            // 
            this.rtbDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbDescricao.Location = new System.Drawing.Point(6, 19);
            this.rtbDescricao.Name = "rtbDescricao";
            this.rtbDescricao.Size = new System.Drawing.Size(436, 87);
            this.rtbDescricao.TabIndex = 11;
            this.rtbDescricao.Text = "";
            // 
            // lblDescricao
            // 
            this.lblDescricao.AutoSize = true;
            this.lblDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescricao.Location = new System.Drawing.Point(-136, -1);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.Size = new System.Drawing.Size(80, 18);
            this.lblDescricao.TabIndex = 10;
            this.lblDescricao.Text = "Descrição:";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.btnVoltar);
            this.groupBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox4.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(371, 502);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(89, 46);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            // 
            // btnVoltar
            // 
            this.btnVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.ForeColor = System.Drawing.Color.White;
            this.btnVoltar.Location = new System.Drawing.Point(6, 14);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(77, 26);
            this.btnVoltar.TabIndex = 10;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = true;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // gpbNome
            // 
            this.gpbNome.BackColor = System.Drawing.Color.Transparent;
            this.gpbNome.Controls.Add(this.lblNomeDeus);
            this.gpbNome.Controls.Add(this.imgDeus);
            this.gpbNome.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbNome.ForeColor = System.Drawing.Color.White;
            this.gpbNome.Location = new System.Drawing.Point(12, 103);
            this.gpbNome.Name = "gpbNome";
            this.gpbNome.Size = new System.Drawing.Size(144, 275);
            this.gpbNome.TabIndex = 2;
            this.gpbNome.TabStop = false;
            this.gpbNome.Text = "Nome:";
            // 
            // lblNomeDeus
            // 
            this.lblNomeDeus.AutoSize = true;
            this.lblNomeDeus.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeDeus.Location = new System.Drawing.Point(6, 18);
            this.lblNomeDeus.Name = "lblNomeDeus";
            this.lblNomeDeus.Size = new System.Drawing.Size(100, 19);
            this.lblNomeDeus.TabIndex = 22;
            this.lblNomeDeus.Text = "Nome do Deus";
            // 
            // imgDeus
            // 
            this.imgDeus.Location = new System.Drawing.Point(7, 40);
            this.imgDeus.Name = "imgDeus";
            this.imgDeus.Size = new System.Drawing.Size(123, 157);
            this.imgDeus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgDeus.TabIndex = 21;
            this.imgDeus.TabStop = false;
            // 
            // gpbHabilidade
            // 
            this.gpbHabilidade.BackColor = System.Drawing.Color.Transparent;
            this.gpbHabilidade.Controls.Add(this.rtbHablidade);
            this.gpbHabilidade.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbHabilidade.ForeColor = System.Drawing.Color.White;
            this.gpbHabilidade.Location = new System.Drawing.Point(164, 104);
            this.gpbHabilidade.Name = "gpbHabilidade";
            this.gpbHabilidade.Size = new System.Drawing.Size(143, 274);
            this.gpbHabilidade.TabIndex = 3;
            this.gpbHabilidade.TabStop = false;
            this.gpbHabilidade.Text = "Habilidade:";
            // 
            // rtbHablidade
            // 
            this.rtbHablidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbHablidade.Location = new System.Drawing.Point(6, 38);
            this.rtbHablidade.Name = "rtbHablidade";
            this.rtbHablidade.Size = new System.Drawing.Size(131, 219);
            this.rtbHablidade.TabIndex = 12;
            this.rtbHablidade.Text = "";
            // 
            // gpbFilhos
            // 
            this.gpbFilhos.BackColor = System.Drawing.Color.Transparent;
            this.gpbFilhos.Controls.Add(this.dtpFilhos);
            this.gpbFilhos.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbFilhos.ForeColor = System.Drawing.Color.White;
            this.gpbFilhos.Location = new System.Drawing.Point(313, 104);
            this.gpbFilhos.Name = "gpbFilhos";
            this.gpbFilhos.Size = new System.Drawing.Size(147, 274);
            this.gpbFilhos.TabIndex = 4;
            this.gpbFilhos.TabStop = false;
            this.gpbFilhos.Text = "Filhos:";
            // 
            // dtpFilhos
            // 
            this.dtpFilhos.AllowUserToAddRows = false;
            this.dtpFilhos.AllowUserToDeleteRows = false;
            this.dtpFilhos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtpFilhos.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.dtpFilhos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtpFilhos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dtpFilhos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtpFilhos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtpFilhos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtpFilhos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Filhos});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtpFilhos.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtpFilhos.GridColor = System.Drawing.Color.Black;
            this.dtpFilhos.Location = new System.Drawing.Point(6, 38);
            this.dtpFilhos.Name = "dtpFilhos";
            this.dtpFilhos.ReadOnly = true;
            this.dtpFilhos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtpFilhos.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtpFilhos.RowHeadersVisible = false;
            this.dtpFilhos.Size = new System.Drawing.Size(131, 219);
            this.dtpFilhos.TabIndex = 27;
            this.dtpFilhos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtpFilhos_CellContentClick);
            // 
            // Filhos
            // 
            this.Filhos.DataPropertyName = "Nome";
            this.Filhos.HeaderText = "Filhos";
            this.Filhos.Name = "Filhos";
            this.Filhos.ReadOnly = true;
            // 
            // frmConsultar_Deus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Acampamento_Meio_Sangue.Properties.Resources.Camp_half_blood;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(469, 556);
            this.ControlBox = false;
            this.Controls.Add(this.gpbFilhos);
            this.Controls.Add(this.gpbHabilidade);
            this.Controls.Add(this.gpbNome);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.gpbDescricao);
            this.Controls.Add(this.gpbConsultarDeuses);
            this.Controls.Add(this.lblFilho);
            this.Controls.Add(this.lblX);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmConsultar_Deus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.gpbConsultarDeuses.ResumeLayout(false);
            this.gpbConsultarDeuses.PerformLayout();
            this.gpbDescricao.ResumeLayout(false);
            this.gpbDescricao.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.gpbNome.ResumeLayout(false);
            this.gpbNome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgDeus)).EndInit();
            this.gpbHabilidade.ResumeLayout(false);
            this.gpbFilhos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtpFilhos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.Label lblFilho;
        private System.Windows.Forms.GroupBox gpbConsultarDeuses;
        private System.Windows.Forms.Label lblDeus;
        private System.Windows.Forms.TextBox txtPesquisa;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.GroupBox gpbDescricao;
        private System.Windows.Forms.RichTextBox rtbDescricao;
        private System.Windows.Forms.Label lblDescricao;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.GroupBox gpbNome;
        private System.Windows.Forms.PictureBox imgDeus;
        private System.Windows.Forms.GroupBox gpbHabilidade;
        private System.Windows.Forms.GroupBox gpbFilhos;
        private System.Windows.Forms.DataGridView dtpFilhos;
        private System.Windows.Forms.Label lblNomeDeus;
        private System.Windows.Forms.RichTextBox rtbHablidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn Filhos;
    }
}