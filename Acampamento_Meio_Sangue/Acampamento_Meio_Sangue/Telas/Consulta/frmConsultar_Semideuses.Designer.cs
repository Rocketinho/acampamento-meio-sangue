﻿namespace Acampamento_Meio_Sangue
{
    partial class frmConsultar_Semideuses
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsultar_Semideuses));
            this.ConsultarSemiDeus = new System.Windows.Forms.GroupBox();
            this.txtPesquisa = new System.Windows.Forms.TextBox();
            this.lblSemiDeus = new System.Windows.Forms.Label();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.gpbDadosSemiDeus = new System.Windows.Forms.GroupBox();
            this.lblChale = new System.Windows.Forms.Label();
            this.lblData = new System.Windows.Forms.Label();
            this.lblDescricao = new System.Windows.Forms.Label();
            this.lblPrimaria = new System.Windows.Forms.Label();
            this.lblSecundária = new System.Windows.Forms.Label();
            this.gpbDadosRecebidos = new System.Windows.Forms.GroupBox();
            this.lblChal3 = new System.Windows.Forms.Label();
            this.lblNascimento = new System.Windows.Forms.Label();
            this.lblDescrica0 = new System.Windows.Forms.Label();
            this.lblArma1 = new System.Windows.Forms.Label();
            this.lblArma2 = new System.Windows.Forms.Label();
            this.gpbNome = new System.Windows.Forms.GroupBox();
            this.lblNomeSemiDeus = new System.Windows.Forms.Label();
            this.imgSemideus = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRemover = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.IDCodigo = new System.Windows.Forms.Label();
            this.Id_chale = new System.Windows.Forms.Label();
            this.iD_DEUS = new System.Windows.Forms.Label();
            this.ConsultarSemiDeus.SuspendLayout();
            this.gpbDadosSemiDeus.SuspendLayout();
            this.gpbDadosRecebidos.SuspendLayout();
            this.gpbNome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSemideus)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ConsultarSemiDeus
            // 
            this.ConsultarSemiDeus.BackColor = System.Drawing.Color.Transparent;
            this.ConsultarSemiDeus.Controls.Add(this.txtPesquisa);
            this.ConsultarSemiDeus.Controls.Add(this.lblSemiDeus);
            this.ConsultarSemiDeus.Controls.Add(this.btnBuscar);
            this.ConsultarSemiDeus.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConsultarSemiDeus.ForeColor = System.Drawing.Color.White;
            this.ConsultarSemiDeus.Location = new System.Drawing.Point(16, 31);
            this.ConsultarSemiDeus.Name = "ConsultarSemiDeus";
            this.ConsultarSemiDeus.Size = new System.Drawing.Size(485, 68);
            this.ConsultarSemiDeus.TabIndex = 1;
            this.ConsultarSemiDeus.TabStop = false;
            this.ConsultarSemiDeus.Text = "Consultar Semi-Deuses: ";
            this.ConsultarSemiDeus.Enter += new System.EventHandler(this.ConsultarSemiDeus_Enter);
            // 
            // txtPesquisa
            // 
            this.txtPesquisa.Location = new System.Drawing.Point(105, 29);
            this.txtPesquisa.Name = "txtPesquisa";
            this.txtPesquisa.Size = new System.Drawing.Size(294, 26);
            this.txtPesquisa.TabIndex = 11;
            // 
            // lblSemiDeus
            // 
            this.lblSemiDeus.AutoSize = true;
            this.lblSemiDeus.BackColor = System.Drawing.Color.Transparent;
            this.lblSemiDeus.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSemiDeus.ForeColor = System.Drawing.Color.White;
            this.lblSemiDeus.Location = new System.Drawing.Point(4, 32);
            this.lblSemiDeus.Name = "lblSemiDeus";
            this.lblSemiDeus.Size = new System.Drawing.Size(106, 18);
            this.lblSemiDeus.TabIndex = 10;
            this.lblSemiDeus.Text = "Semi-Deuses: ";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.ForeColor = System.Drawing.Color.White;
            this.btnBuscar.Location = new System.Drawing.Point(405, 29);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 26);
            this.btnBuscar.TabIndex = 8;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // gpbDadosSemiDeus
            // 
            this.gpbDadosSemiDeus.BackColor = System.Drawing.Color.Transparent;
            this.gpbDadosSemiDeus.Controls.Add(this.lblChale);
            this.gpbDadosSemiDeus.Controls.Add(this.lblData);
            this.gpbDadosSemiDeus.Controls.Add(this.lblDescricao);
            this.gpbDadosSemiDeus.Controls.Add(this.lblPrimaria);
            this.gpbDadosSemiDeus.Controls.Add(this.lblSecundária);
            this.gpbDadosSemiDeus.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbDadosSemiDeus.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gpbDadosSemiDeus.Location = new System.Drawing.Point(16, 113);
            this.gpbDadosSemiDeus.Name = "gpbDadosSemiDeus";
            this.gpbDadosSemiDeus.Size = new System.Drawing.Size(163, 191);
            this.gpbDadosSemiDeus.TabIndex = 2;
            this.gpbDadosSemiDeus.TabStop = false;
            this.gpbDadosSemiDeus.Text = "Dados do Semi-Deus";
            // 
            // lblChale
            // 
            this.lblChale.AutoSize = true;
            this.lblChale.BackColor = System.Drawing.Color.Transparent;
            this.lblChale.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChale.ForeColor = System.Drawing.Color.White;
            this.lblChale.Location = new System.Drawing.Point(8, 95);
            this.lblChale.Name = "lblChale";
            this.lblChale.Size = new System.Drawing.Size(50, 18);
            this.lblChale.TabIndex = 24;
            this.lblChale.Text = "Chalé:";
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.BackColor = System.Drawing.Color.Transparent;
            this.lblData.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.ForeColor = System.Drawing.Color.White;
            this.lblData.Location = new System.Drawing.Point(8, 161);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(147, 18);
            this.lblData.TabIndex = 23;
            this.lblData.Text = "Data de Nascimento:";
            // 
            // lblDescricao
            // 
            this.lblDescricao.AutoSize = true;
            this.lblDescricao.BackColor = System.Drawing.Color.Transparent;
            this.lblDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescricao.ForeColor = System.Drawing.Color.White;
            this.lblDescricao.Location = new System.Drawing.Point(8, 128);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.Size = new System.Drawing.Size(80, 18);
            this.lblDescricao.TabIndex = 22;
            this.lblDescricao.Text = "Descrição:";
            // 
            // lblPrimaria
            // 
            this.lblPrimaria.AutoSize = true;
            this.lblPrimaria.BackColor = System.Drawing.Color.Transparent;
            this.lblPrimaria.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrimaria.ForeColor = System.Drawing.Color.White;
            this.lblPrimaria.Location = new System.Drawing.Point(7, 29);
            this.lblPrimaria.Name = "lblPrimaria";
            this.lblPrimaria.Size = new System.Drawing.Size(106, 18);
            this.lblPrimaria.TabIndex = 21;
            this.lblPrimaria.Text = "Arma Primária:";
            // 
            // lblSecundária
            // 
            this.lblSecundária.AutoSize = true;
            this.lblSecundária.BackColor = System.Drawing.Color.Transparent;
            this.lblSecundária.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSecundária.ForeColor = System.Drawing.Color.White;
            this.lblSecundária.Location = new System.Drawing.Point(8, 62);
            this.lblSecundária.Name = "lblSecundária";
            this.lblSecundária.Size = new System.Drawing.Size(125, 18);
            this.lblSecundária.TabIndex = 20;
            this.lblSecundária.Text = "Arma Secundaria:";
            // 
            // gpbDadosRecebidos
            // 
            this.gpbDadosRecebidos.BackColor = System.Drawing.Color.Transparent;
            this.gpbDadosRecebidos.Controls.Add(this.lblChal3);
            this.gpbDadosRecebidos.Controls.Add(this.lblNascimento);
            this.gpbDadosRecebidos.Controls.Add(this.lblDescrica0);
            this.gpbDadosRecebidos.Controls.Add(this.lblArma1);
            this.gpbDadosRecebidos.Controls.Add(this.lblArma2);
            this.gpbDadosRecebidos.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbDadosRecebidos.ForeColor = System.Drawing.Color.White;
            this.gpbDadosRecebidos.Location = new System.Drawing.Point(185, 113);
            this.gpbDadosRecebidos.Name = "gpbDadosRecebidos";
            this.gpbDadosRecebidos.Size = new System.Drawing.Size(163, 191);
            this.gpbDadosRecebidos.TabIndex = 3;
            this.gpbDadosRecebidos.TabStop = false;
            this.gpbDadosRecebidos.Text = "Dados recebidos:";
            // 
            // lblChal3
            // 
            this.lblChal3.AutoSize = true;
            this.lblChal3.BackColor = System.Drawing.Color.Transparent;
            this.lblChal3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChal3.ForeColor = System.Drawing.Color.White;
            this.lblChal3.Location = new System.Drawing.Point(8, 95);
            this.lblChal3.Name = "lblChal3";
            this.lblChal3.Size = new System.Drawing.Size(46, 18);
            this.lblChal3.TabIndex = 24;
            this.lblChal3.Text = "Chalé";
            // 
            // lblNascimento
            // 
            this.lblNascimento.AutoSize = true;
            this.lblNascimento.BackColor = System.Drawing.Color.Transparent;
            this.lblNascimento.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNascimento.ForeColor = System.Drawing.Color.White;
            this.lblNascimento.Location = new System.Drawing.Point(8, 161);
            this.lblNascimento.Name = "lblNascimento";
            this.lblNascimento.Size = new System.Drawing.Size(143, 18);
            this.lblNascimento.TabIndex = 23;
            this.lblNascimento.Text = "Data de Nascimento";
            // 
            // lblDescrica0
            // 
            this.lblDescrica0.AutoSize = true;
            this.lblDescrica0.BackColor = System.Drawing.Color.Transparent;
            this.lblDescrica0.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescrica0.ForeColor = System.Drawing.Color.White;
            this.lblDescrica0.Location = new System.Drawing.Point(8, 128);
            this.lblDescrica0.Name = "lblDescrica0";
            this.lblDescrica0.Size = new System.Drawing.Size(76, 18);
            this.lblDescrica0.TabIndex = 22;
            this.lblDescrica0.Text = "Descrição";
            // 
            // lblArma1
            // 
            this.lblArma1.AutoSize = true;
            this.lblArma1.BackColor = System.Drawing.Color.Transparent;
            this.lblArma1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArma1.ForeColor = System.Drawing.Color.White;
            this.lblArma1.Location = new System.Drawing.Point(7, 29);
            this.lblArma1.Name = "lblArma1";
            this.lblArma1.Size = new System.Drawing.Size(102, 18);
            this.lblArma1.TabIndex = 21;
            this.lblArma1.Text = "Arma Primária";
            // 
            // lblArma2
            // 
            this.lblArma2.AutoSize = true;
            this.lblArma2.BackColor = System.Drawing.Color.Transparent;
            this.lblArma2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArma2.ForeColor = System.Drawing.Color.White;
            this.lblArma2.Location = new System.Drawing.Point(8, 62);
            this.lblArma2.Name = "lblArma2";
            this.lblArma2.Size = new System.Drawing.Size(121, 18);
            this.lblArma2.TabIndex = 20;
            this.lblArma2.Text = "Arma Secundaria";
            // 
            // gpbNome
            // 
            this.gpbNome.BackColor = System.Drawing.Color.Transparent;
            this.gpbNome.Controls.Add(this.lblNomeSemiDeus);
            this.gpbNome.Controls.Add(this.imgSemideus);
            this.gpbNome.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbNome.ForeColor = System.Drawing.Color.White;
            this.gpbNome.Location = new System.Drawing.Point(365, 113);
            this.gpbNome.Name = "gpbNome";
            this.gpbNome.Size = new System.Drawing.Size(136, 191);
            this.gpbNome.TabIndex = 4;
            this.gpbNome.TabStop = false;
            this.gpbNome.Text = "Nome:";
            // 
            // lblNomeSemiDeus
            // 
            this.lblNomeSemiDeus.AutoSize = true;
            this.lblNomeSemiDeus.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeSemiDeus.Location = new System.Drawing.Point(6, 23);
            this.lblNomeSemiDeus.Name = "lblNomeSemiDeus";
            this.lblNomeSemiDeus.Size = new System.Drawing.Size(108, 18);
            this.lblNomeSemiDeus.TabIndex = 15;
            this.lblNomeSemiDeus.Text = "Nome Semi-Deus";
            // 
            // imgSemideus
            // 
            this.imgSemideus.Location = new System.Drawing.Point(6, 45);
            this.imgSemideus.Name = "imgSemideus";
            this.imgSemideus.Size = new System.Drawing.Size(125, 140);
            this.imgSemideus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgSemideus.TabIndex = 14;
            this.imgSemideus.TabStop = false;
            this.imgSemideus.Click += new System.EventHandler(this.imgSemideus_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.groupBox5.Controls.Add(this.btnVoltar);
            this.groupBox5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox5.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(412, 310);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(89, 46);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            // 
            // btnVoltar
            // 
            this.btnVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.ForeColor = System.Drawing.Color.White;
            this.btnVoltar.Location = new System.Drawing.Point(6, 14);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(77, 26);
            this.btnVoltar.TabIndex = 10;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = true;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.groupBox1.Controls.Add(this.btnAlterar);
            this.groupBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox1.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(317, 310);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(89, 46);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // btnAlterar
            // 
            this.btnAlterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlterar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar.ForeColor = System.Drawing.Color.White;
            this.btnAlterar.Location = new System.Drawing.Point(6, 14);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(77, 26);
            this.btnAlterar.TabIndex = 10;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.groupBox2.Controls.Add(this.btnRemover);
            this.groupBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox2.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(225, 310);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(89, 46);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // btnRemover
            // 
            this.btnRemover.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRemover.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemover.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemover.ForeColor = System.Drawing.Color.White;
            this.btnRemover.Location = new System.Drawing.Point(6, 14);
            this.btnRemover.Name = "btnRemover";
            this.btnRemover.Size = new System.Drawing.Size(77, 26);
            this.btnRemover.TabIndex = 10;
            this.btnRemover.Text = "Remover";
            this.btnRemover.UseVisualStyleBackColor = true;
            this.btnRemover.Click += new System.EventHandler(this.btnRemover_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(476, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 24);
            this.label1.TabIndex = 13;
            this.label1.Text = "X";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // IDCodigo
            // 
            this.IDCodigo.AutoSize = true;
            this.IDCodigo.Location = new System.Drawing.Point(13, 4);
            this.IDCodigo.Name = "IDCodigo";
            this.IDCodigo.Size = new System.Drawing.Size(10, 13);
            this.IDCodigo.TabIndex = 14;
            this.IDCodigo.Text = "-";
            this.IDCodigo.Visible = false;
            // 
            // Id_chale
            // 
            this.Id_chale.AutoSize = true;
            this.Id_chale.Location = new System.Drawing.Point(42, 4);
            this.Id_chale.Name = "Id_chale";
            this.Id_chale.Size = new System.Drawing.Size(10, 13);
            this.Id_chale.TabIndex = 15;
            this.Id_chale.Text = "-";
            this.Id_chale.Visible = false;
            // 
            // iD_DEUS
            // 
            this.iD_DEUS.AutoSize = true;
            this.iD_DEUS.Location = new System.Drawing.Point(64, 4);
            this.iD_DEUS.Name = "iD_DEUS";
            this.iD_DEUS.Size = new System.Drawing.Size(10, 13);
            this.iD_DEUS.TabIndex = 16;
            this.iD_DEUS.Text = "-";
            this.iD_DEUS.Visible = false;
            // 
            // frmConsultar_Semideuses
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Acampamento_Meio_Sangue.Properties.Resources.Camp_half_blood;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(515, 368);
            this.ControlBox = false;
            this.Controls.Add(this.iD_DEUS);
            this.Controls.Add(this.Id_chale);
            this.Controls.Add(this.IDCodigo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.gpbNome);
            this.Controls.Add(this.gpbDadosRecebidos);
            this.Controls.Add(this.gpbDadosSemiDeus);
            this.Controls.Add(this.ConsultarSemiDeus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmConsultar_Semideuses";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmConsultar_Semideuses_Load);
            this.ConsultarSemiDeus.ResumeLayout(false);
            this.ConsultarSemiDeus.PerformLayout();
            this.gpbDadosSemiDeus.ResumeLayout(false);
            this.gpbDadosSemiDeus.PerformLayout();
            this.gpbDadosRecebidos.ResumeLayout(false);
            this.gpbDadosRecebidos.PerformLayout();
            this.gpbNome.ResumeLayout(false);
            this.gpbNome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSemideus)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox ConsultarSemiDeus;
        private System.Windows.Forms.Label lblSemiDeus;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.GroupBox gpbDadosSemiDeus;
        private System.Windows.Forms.Label lblChale;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Label lblDescricao;
        private System.Windows.Forms.Label lblPrimaria;
        private System.Windows.Forms.Label lblSecundária;
        private System.Windows.Forms.GroupBox gpbDadosRecebidos;
        private System.Windows.Forms.Label lblChal3;
        private System.Windows.Forms.Label lblNascimento;
        private System.Windows.Forms.Label lblDescrica0;
        private System.Windows.Forms.Label lblArma1;
        private System.Windows.Forms.Label lblArma2;
        private System.Windows.Forms.GroupBox gpbNome;
        private System.Windows.Forms.Label lblNomeSemiDeus;
        private System.Windows.Forms.PictureBox imgSemideus;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.TextBox txtPesquisa;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnRemover;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label IDCodigo;
        private System.Windows.Forms.Label Id_chale;
        private System.Windows.Forms.Label iD_DEUS;
    }
}