﻿namespace Acampamento_Meio_Sangue
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblSenha = new System.Windows.Forms.Label();
            this.lblX = new System.Windows.Forms.Label();
            this.cboDeus = new System.Windows.Forms.ComboBox();
            this.lblDeus = new System.Windows.Forms.Label();
            this.gpbRecuperar = new System.Windows.Forms.GroupBox();
            this.lblClickAqui = new System.Windows.Forms.Label();
            this.lblEsqueceu = new System.Windows.Forms.Label();
            this.btnEntrar = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.gpbRecuperar.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(93, 85);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(164, 22);
            this.txtPassword.TabIndex = 2;
            this.txtPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPassword_KeyDown_1);
            // 
            // lblSenha
            // 
            this.lblSenha.AutoSize = true;
            this.lblSenha.BackColor = System.Drawing.Color.GreenYellow;
            this.lblSenha.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSenha.ForeColor = System.Drawing.Color.Black;
            this.lblSenha.Location = new System.Drawing.Point(21, 84);
            this.lblSenha.Name = "lblSenha";
            this.lblSenha.Size = new System.Drawing.Size(60, 21);
            this.lblSenha.TabIndex = 1;
            this.lblSenha.Text = "Senha:";
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.BackColor = System.Drawing.Color.Transparent;
            this.lblX.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblX.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblX.ForeColor = System.Drawing.Color.White;
            this.lblX.Location = new System.Drawing.Point(232, 9);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(25, 24);
            this.lblX.TabIndex = 6;
            this.lblX.Text = "X";
            this.lblX.Click += new System.EventHandler(this.lblX_Click);
            // 
            // cboDeus
            // 
            this.cboDeus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDeus.FormattingEnabled = true;
            this.cboDeus.Location = new System.Drawing.Point(93, 58);
            this.cboDeus.Name = "cboDeus";
            this.cboDeus.Size = new System.Drawing.Size(164, 21);
            this.cboDeus.TabIndex = 1;
            // 
            // lblDeus
            // 
            this.lblDeus.AutoSize = true;
            this.lblDeus.BackColor = System.Drawing.Color.GreenYellow;
            this.lblDeus.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeus.ForeColor = System.Drawing.Color.Black;
            this.lblDeus.Location = new System.Drawing.Point(21, 55);
            this.lblDeus.Name = "lblDeus";
            this.lblDeus.Size = new System.Drawing.Size(51, 21);
            this.lblDeus.TabIndex = 9;
            this.lblDeus.Text = "Deus:";
            // 
            // gpbRecuperar
            // 
            this.gpbRecuperar.BackColor = System.Drawing.Color.Transparent;
            this.gpbRecuperar.Controls.Add(this.lblClickAqui);
            this.gpbRecuperar.Controls.Add(this.lblEsqueceu);
            this.gpbRecuperar.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbRecuperar.ForeColor = System.Drawing.Color.YellowGreen;
            this.gpbRecuperar.Location = new System.Drawing.Point(25, 162);
            this.gpbRecuperar.Name = "gpbRecuperar";
            this.gpbRecuperar.Size = new System.Drawing.Size(232, 49);
            this.gpbRecuperar.TabIndex = 5;
            this.gpbRecuperar.TabStop = false;
            this.gpbRecuperar.Text = "Importante!";
            // 
            // lblClickAqui
            // 
            this.lblClickAqui.AutoSize = true;
            this.lblClickAqui.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblClickAqui.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClickAqui.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblClickAqui.Location = new System.Drawing.Point(139, 20);
            this.lblClickAqui.Name = "lblClickAqui";
            this.lblClickAqui.Size = new System.Drawing.Size(89, 17);
            this.lblClickAqui.TabIndex = 0;
            this.lblClickAqui.Text = "Click Aqui";
            this.lblClickAqui.Click += new System.EventHandler(this.lblClickAqui_Click);
            // 
            // lblEsqueceu
            // 
            this.lblEsqueceu.AutoSize = true;
            this.lblEsqueceu.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEsqueceu.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblEsqueceu.Location = new System.Drawing.Point(7, 20);
            this.lblEsqueceu.Name = "lblEsqueceu";
            this.lblEsqueceu.Size = new System.Drawing.Size(126, 19);
            this.lblEsqueceu.TabIndex = 16;
            this.lblEsqueceu.Text = "Esqueceu a senha?";
            // 
            // btnEntrar
            // 
            this.btnEntrar.BackColor = System.Drawing.Color.GreenYellow;
            this.btnEntrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEntrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEntrar.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEntrar.ForeColor = System.Drawing.Color.Black;
            this.btnEntrar.Location = new System.Drawing.Point(197, 130);
            this.btnEntrar.Name = "btnEntrar";
            this.btnEntrar.Size = new System.Drawing.Size(60, 26);
            this.btnEntrar.TabIndex = 3;
            this.btnEntrar.Text = "Entrar";
            this.btnEntrar.UseVisualStyleBackColor = false;
            this.btnEntrar.Click += new System.EventHandler(this.btnEntrar_Click);
            // 
            // btnSair
            // 
            this.btnSair.BackColor = System.Drawing.Color.GreenYellow;
            this.btnSair.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSair.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.ForeColor = System.Drawing.Color.Black;
            this.btnSair.Location = new System.Drawing.Point(135, 130);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(60, 26);
            this.btnSair.TabIndex = 4;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = false;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Acampamento_Meio_Sangue.Properties.Resources.telalogin;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(267, 229);
            this.ControlBox = false;
            this.Controls.Add(this.btnSair);
            this.Controls.Add(this.btnEntrar);
            this.Controls.Add(this.gpbRecuperar);
            this.Controls.Add(this.lblDeus);
            this.Controls.Add(this.cboDeus);
            this.Controls.Add(this.lblX);
            this.Controls.Add(this.lblSenha);
            this.Controls.Add(this.txtPassword);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLogin";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.gpbRecuperar.ResumeLayout(false);
            this.gpbRecuperar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblSenha;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.ComboBox cboDeus;
        private System.Windows.Forms.Label lblDeus;
        private System.Windows.Forms.GroupBox gpbRecuperar;
        private System.Windows.Forms.Label lblClickAqui;
        private System.Windows.Forms.Label lblEsqueceu;
        private System.Windows.Forms.Button btnEntrar;
        private System.Windows.Forms.Button btnSair;
    }
}

