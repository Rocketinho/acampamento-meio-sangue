﻿using Acampamento_Meio_Sangue.Chale;
using Acampamento_Meio_Sangue.DB.Login;
using Acampamento_Meio_Sangue.DB.Login.Criptografia.Hash;
using Acampamento_Meio_Sangue.Deus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acampamento_Meio_Sangue
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
            CarregarCombo();
            
        }

        public static string UsuarioConectado;
        public static string NivelAcesso;

        public void CarregarCombo()
        {
           Business_Deus data = new Business_Deus();
           List<DTO_Deus> listar = data.Listar();

            cboDeus.ValueMember = nameof(DTO_Deus.ID_Deus);
            cboDeus.DisplayMember = nameof(DTO_Deus.Deus);

            cboDeus.DataSource = listar;
        }
        public void Magicamente ()
        {
            DTO_Deus deus = cboDeus.SelectedItem as DTO_Deus;
            
            DTO_Usuario dto = new DTO_Usuario();
            dto.Senha = txtPassword.Text;
            dto.ID_Deus = deus.ID_Deus;

            Business_Usuario db = new Business_Usuario();
            DTO_Usuario usuario = db.Log(dto);

            if (usuario != null)
            {
                UserSession.UsuarioLogado = usuario;
                
                TelaMenu menu = new TelaMenu();
                this.Hide();
                menu.Show();
                
                                
            }
            else
            {
                MessageBox.Show("Credênciais inválidas!!",
                                    "Acampamento Meio-Sangue",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
            }
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            Magicamente();
        }

        private void txtPassword_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Magicamente();
            }
        }

        private void lblX_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja sair do Acampamento?",
                                                 "Acapamento Meio-sangue",
                                                 MessageBoxButtons.YesNo,
                                                 MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja sair do Acampamento?",
                                                 "Acapamento Meio-sangue",
                                                 MessageBoxButtons.YesNo,
                                                 MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void lblClickAqui_Click(object sender, EventArgs e)
        {
            frmGluglu tela = new frmGluglu();
            tela.Show();
            this.Hide();
        }
    }
}
