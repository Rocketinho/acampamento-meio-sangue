﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acampamento_Meio_Sangue
{
    public partial class frmGluglu : Form
    {
        public frmGluglu()
        {
            InitializeComponent();
            
        }

        private void label1_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Voltando para a parte de login, tudo bem?",
                                                  "Acampamento Meio-Sangue",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Information);
            if (dialog == DialogResult.Yes)
            {
                frmLogin tela = new frmLogin();
                tela.Show();
                this.Close();
            }
        }

        private void frmGluglu_Load(object sender, EventArgs e)
        {
            MessageBox.Show("Não é possível alterar a senha, pois o cadastro é feito a mando do professor",
                            "Acampamento Meio-Sangue",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }
    }
}
